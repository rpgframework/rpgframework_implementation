/**
 * 
 */
package org.prelle.rpgframework.genericrpg;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.rpgframework.genericrpg.ModificationController;
import de.rpgframework.genericrpg.ModifyableValue;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author Stefan
 *
 */
public abstract class BaseModificationController<T> implements ModificationController<T> {

	protected Map<T, ValueModification<T>> stacks;
	protected List<ModifyableValue<T>> available;
	
	//--------------------------------------------------------------------
	public BaseModificationController() {
		stacks = new HashMap<T, ValueModification<T>>();
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModificationController#getSelected()
	 */
	@Override
	public List<ModifyableValue<T>> getSelected() {
		// TODO Auto-generated method stub
		return null;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModificationController#increase(de.rpgframework.genericrpg.ModifyableValue)
	 */
	@Override
	public boolean increase(ModifyableValue<T> value) {
		if (!canBeIncreased(value))
			return false;
		
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModificationController#decrease(de.rpgframework.genericrpg.ModifyableValue)
	 */
	@Override
	public boolean decrease(ModifyableValue<T> value) {
		// TODO Auto-generated method stub
		return false;
	}

}
