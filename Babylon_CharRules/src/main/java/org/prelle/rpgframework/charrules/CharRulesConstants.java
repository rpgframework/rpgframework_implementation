/**
 * 
 */
package org.prelle.rpgframework.charrules;

import java.util.ResourceBundle;

/**
 * @author prelle
 *
 */
public interface CharRulesConstants {
	
	public final static ResourceBundle RES = ResourceBundle.getBundle("org/prelle/rpgframework/charrules/i18n/babylon");

}
