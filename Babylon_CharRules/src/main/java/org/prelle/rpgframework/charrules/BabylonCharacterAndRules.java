/**
 * 
 */
package org.prelle.rpgframework.charrules;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.rpgframework.BabylonPlugin;
import org.prelle.rpgframework.ConfigContainerImpl;
import org.prelle.rpgframework.RPGFrameworkImpl;
import org.prelle.rpgframework.character.BaseCharacterProviderLight;
import org.prelle.rpgframework.character.EventingCharacterProviderLight;
import org.prelle.rpgframework.print.PrintManagerImpl;
import org.prelle.rpgframework.products.ProductServiceImpl;
import org.prelle.rpgframework.steps.LoadProductsBootStep;
import org.prelle.rpgframework.steps.LoadRulePluginsBootStep;

import de.rpgframework.FunctionCharacterAndRules;
import de.rpgframework.RPGFrameworkInitCallback;
import de.rpgframework.RPGFrameworkLoader.FunctionType;
import de.rpgframework.RulePlugin;
import de.rpgframework.boot.StandardBootSteps;
import de.rpgframework.character.CharacterProvider;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.print.PrintManager;
import de.rpgframework.products.ProductService;

/**
 * @author prelle
 *
 */
public class BabylonCharacterAndRules implements BabylonPlugin, FunctionCharacterAndRules {
	
	private final static Logger logger = LogManager.getLogger("babylon.car");

	private CharacterProvider characterService;
	private ProductServiceImpl productService;
	private PrintManagerImpl printService;
	
	@SuppressWarnings("unused")
	private RPGFrameworkInitCallback callback;
	
	private LoadRulePluginsBootStep rulePlugins;

	//-------------------------------------------------------------------
	public BabylonCharacterAndRules() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.BabylonPlugin#getType()
	 */
	@Override
	public FunctionType getType() {
		return FunctionType.CHARACTERS_AND_RULES;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.BabylonPlugin#initialize(org.prelle.rpgframework.RPGFrameworkImpl, de.rpgframework.ConfigContainer, de.rpgframework.RPGFrameworkInitCallback)
	 */
	@Override
	public void initialize(RPGFrameworkImpl fwImpl, ConfigContainerImpl configRoot, RPGFrameworkInitCallback callback, List<RoleplayingSystem> limit) {
		logger.debug("START: initialize");
		this.callback = callback;
//		this.configRoot = configRoot;
		try {
			characterService = new EventingCharacterProviderLight(new BaseCharacterProviderLight(configRoot));
			productService   = new ProductServiceImpl(configRoot);
			printService     = new PrintManagerImpl(configRoot);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
		fwImpl.setCharacterAndRules(this);

//		loadRulePlugins(fwImpl, limit);
//		
//		productService.initialize();

		rulePlugins = new LoadRulePluginsBootStep(configRoot);
		fwImpl.addStepDefinition(StandardBootSteps.ROLEPLAYING_SYSTEMS, rulePlugins);
		fwImpl.addStepDefinition(StandardBootSteps.PRODUCT_DATA, new LoadProductsBootStep(productService));
		logger.debug("STOP : initialize");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.FunctionCharacterAndRules#getRulePlugins()
	 */
	@Override
	public List<RulePlugin<?>> getRulePlugins() {
		return rulePlugins.getRulePlugins();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.FunctionCharacterAndRules#getCharacterService()
	 */
	@Override
	public CharacterProvider getCharacterService() {
		return characterService;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.FunctionCharacterAndRules#getProductService()
	 */
	@Override
	public ProductService getProductService() {
		return productService;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.FunctionCharacterAndRules#getPrintManager()
	 */
	@Override
	public PrintManager getPrintManager() {
		return printService;
	}

}
