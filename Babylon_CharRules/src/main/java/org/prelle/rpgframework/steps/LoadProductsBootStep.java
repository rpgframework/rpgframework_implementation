/**
 *
 */
package org.prelle.rpgframework.steps;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ServiceLoader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.rpgframework.products.ProductDataPlugin;
import org.prelle.rpgframework.products.ProductServiceImpl;

import de.rpgframework.ConfigOption;
import de.rpgframework.RPGFrameworkInitCallback;
import de.rpgframework.RPGFrameworkLoader;
import de.rpgframework.boot.BootStep;

/**
 * @author Stefan
 *
 */
public class LoadProductsBootStep implements BootStep {

	private final static Logger logger = LogManager.getLogger("babylon.car");

	private ProductServiceImpl products;

	//-------------------------------------------------------------------
	public LoadProductsBootStep(ProductServiceImpl products) {
		this.products = products;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.boot.BootStep#getID()
	 */
	@Override
	public String getID() {
		return "PRODUCT_DATA";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.boot.BootStep#getWeight()
	 */
	@Override
	public int getWeight() {
		return 20;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.boot.BootStep#shallBeDisplayedToUser()
	 */
	@Override
	public boolean shallBeDisplayedToUser() {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.boot.BootStep#getConfiguration()
	 */
	@Override
	public List<ConfigOption<?>> getConfiguration() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.boot.BootStep#execute()
	 */
	@Override
	public boolean execute(RPGFrameworkInitCallback callback) {
		logger.debug("START----------Load product data------------------------");
		try {
			logger.info("Call ProductServiceImpl.initialize()");
			products.initialize();
			
			/*
			 * Check for embedded product services
			 */
			Iterator<ProductDataPlugin> it = ServiceLoader.load(ProductDataPlugin.class).iterator();
			while (it.hasNext()) {
				ProductDataPlugin plugin = it.next();
				logger.info("Found product plugin "+plugin.getClass().getSimpleName());
				plugin.initialize(RPGFrameworkLoader.getInstance(), products);
			}
			
		} catch (Throwable e) {
			logger.error("Failed loading product data",e);
			return false;
		} finally {
			logger.debug("STOP: loadProductData--------------------------");
		}
		return false;
	}

}
