/**
 * @author Stefan Prelle
 *
 */
module babylon.charrules {
	exports org.prelle.rpgframework.character;
	exports org.prelle.rpgframework.charrules;
	exports org.prelle.rpgframework.core;
	exports org.prelle.rpgframework.steps;
	exports org.prelle.rpgframework.genericrpg.modification;
	exports org.prelle.rpgframework.print;
	exports org.prelle.rpgframework.genericrpg;
	exports org.prelle.rpgframework.products;

//	uses org.prelle.rpgframework.BabylonPlugin;
	uses de.rpgframework.RulePlugin;
	uses org.prelle.rpgframework.products.ProductDataPlugin;
	provides org.prelle.rpgframework.BabylonPlugin with org.prelle.rpgframework.charrules.BabylonCharacterAndRules;

	opens org.prelle.rpgframework.products to simple.persist;
	opens org.prelle.rpgframework.print to simple.persist;

	requires babylon;
	requires java.sql;
	requires java.sql.rowset;
	requires java.xml;
	requires org.apache.logging.log4j;
	requires transitive rpgframework.api;
	requires simple.persist;
}