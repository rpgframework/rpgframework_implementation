package foo;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.prelle.rpgframework.products.AdventureImpl;
import org.prelle.rpgframework.products.AdventureStoryImpl;
import org.prelle.rpgframework.products.ProductImpl;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.SerializationException;
import org.prelle.simplepersist.Serializer;

import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.products.AdventureStory;

public class PersistAdventureTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() throws SerializationException, IOException {
//		BasicConfigurator.configure();
//		ConsoleAppender append = new ConsoleAppender(new PatternLayout("%5p [%c] (%F:%L) - %m%n"));
//		LogManager.getRootLogger().addAppender(append);
		AdventureStory story = new AdventureStoryImpl("story");
		AdventureImpl impl1 = new AdventureImpl("testadv", RoleplayingSystem.CORIOLIS, story, null);
		AdventureImpl impl2 = new AdventureImpl("testadv2", RoleplayingSystem.SHADOWRUN, story, null);
		ProductImpl prod = new ProductImpl();
		prod.addAdventure(impl1);
		prod.addAdventure(impl2);
		
		Serializer persist = new Persister();
		persist.write(prod, System.out);
	}

}
