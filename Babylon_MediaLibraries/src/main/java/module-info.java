/**
 * @author Stefan Prelle
 *
 */
module babylon.media {
	exports org.prelle.rpgframework.media.map;
	exports org.prelle.rpgframework.media;
	exports org.prelle.rpgframework.media.metadata;
	exports org.prelle.rpgframework.media.collector;
	exports org.prelle.rpgframework.music;

	provides org.prelle.rpgframework.BabylonPlugin with org.prelle.rpgframework.media.BabylonMediaLibraries;

	requires babylon;
	requires com.google.gson;
	requires java.desktop;
	requires java.prefs;
	requires java.sql;
	requires java.xml;
	requires javafx.base;
	requires javafx.graphics;
	requires javafx.swing;
	requires javafx.web;
	requires jcodec;
	requires jcodec.javase;
	requires libid3;
	requires metadata.extractor;
	requires org.apache.commons.imaging;
	requires org.apache.logging.log4j;
	requires org.apache.pdfbox;
	requires rpgframework.api;
	requires simple.persist;
}