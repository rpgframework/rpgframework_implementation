/**
 * 
 */
package org.prelle.rpgframework.music;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.myid3lib.TagException;
import org.prelle.myid3lib.TagReader;
import org.prelle.myid3lib.TaggedFile;
import org.prelle.myid3lib.frame.ID3FrameType;

import de.rpgframework.core.EventBus;
import de.rpgframework.core.EventType;
import de.rpgframework.music.ScanResult;
import de.rpgframework.music.ScanResult.State;
import de.rpgframework.music.Track;
import de.rpgframework.music.TrackRepository;
import de.rpgframework.music.UniqueTrackID;

/**
 * @author Stefan
 *
 */
public class FileSystemTrackRepository implements TrackRepository {

	private final static Logger logger = LogManager.getLogger("babylon.music");
	
	private FileSystem filesystem;
	private Path root;
	private Map<String, Track> knownTracks;
	
	//--------------------------------------------------------------------
	/**
	 */
	public FileSystemTrackRepository(Path root) {
		this.filesystem = root.getFileSystem();
		this.root = root;
		knownTracks = new HashMap<String, Track>();

		initiateScan();
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.music.TrackRepository#initiateScan()
	 */
	@Override
	public void initiateScan() {		
		Thread scanThread = new Thread(new Runnable(){
			@Override
			public void run() {
				List<Path> files = getAllFiles(root);
				scanFiles(files);
			}},
				"MusicScan-"+filesystem);
		scanThread.run();
	}

	//--------------------------------------------------------------------
	private List<Path> getAllFiles(Path dir) {
		List<Path> ret = new ArrayList<Path>();
		
		try {
			Stream<Path> children = Files.list(dir);
			Iterator<Path> it = children.iterator();
			while (it.hasNext()) {
				Path child = it.next();
				if (!Files.isReadable(child)) {
					logger.warn("Not readable: "+child);
					continue;
				}
				if (logger.isTraceEnabled())
					logger.trace("* "+root.relativize(child));
				if (Files.isRegularFile(child ) && child.getFileName().toString().endsWith(".mp3")) {
					ret.add(child);
				} else if (Files.isDirectory(child))
					ret.addAll(getAllFiles(child));
			}
			children.close();
		} catch (IOException e) {
			logger.error("Failed getting directory listing of "+dir,e);
		}
		
		return ret;
	}

	//--------------------------------------------------------------------
	private void  scanFiles(List<Path> files) {
		logger.debug("scan "+files.size()+" files");
		int count = 0;
		List<ScanResult> results = new ArrayList<>();
		long lastUpdate = System.currentTimeMillis();
		for (Path file : files) {						
			count++;
			
			ScanResult.State state = State.MISSING_TAG;
			
			String relPath = root.relativize(file).toString();
			if (!knownTracks.containsKey(relPath)) {
				// Read ID3 tags
				try {
					logger.debug("---------"+relPath+"---------------");
					TaggedFile tagged = TagReader.read(file);
					if (tagged.getV1()==null && tagged.getV2()==null) {
						results.add(new FileSystemScanResult(new FileSystemTrack(file, tagged, null), state));
						continue;
					}
					// There is at least a ID3 tag - now search for MBID
					state = State.MISSING_MUSICBRAINZ_ID;
					
					if (tagged.getV2()==null) {
						logger.debug("No ID3v2 tag found in "+file);
						results.add(new FileSystemScanResult(new FileSystemTrack(file, tagged, null), state));
						continue;
					}
					
					String mbID = tagged.getV2().getAsString(ID3FrameType.UFID); // Unique
					if (mbID==null) {
						logger.warn("No 'MusicBrainz Id' found in "+file);
						results.add(new FileSystemScanResult(new FileSystemTrack(file, tagged, null), state));
						continue;
					} else {
						BabylonUniqueTrackID uniqID = new BabylonUniqueTrackID(mbID);
						FileSystemTrack track = new FileSystemTrack(file, tagged, uniqID);
						knownTracks.put(relPath, track);
						results.add(new FileSystemScanResult(track, State.MISSING_CLASSIFICATION));
					}
					
				} catch (IOException e) {
					logger.error("Failed reading MP3",e);
				} catch (TagException e) {
					logger.error("Failed parsing ID3 tags",e);
				}
			
				
			}
			// Inform listeners every second
			if ((System.currentTimeMillis()-lastUpdate)>2000) {
				double percent = count / ((double)files.size()) * 100.0;
				logger.debug(String.format("Scan progress: %f", percent));
				EventBus.fireEvent(this, EventType.MUSIC_SCAN_PROGRESS, filesystem, percent);
				lastUpdate = System.currentTimeMillis();
			}
		}
		EventBus.fireEvent(this, EventType.MUSIC_SCAN_PROGRESS, filesystem, 100.0);
		EventBus.fireEvent(this, EventType.MUSIC_SCAN_COMPLETE, filesystem, results);
		logger.debug("scan complete");
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.music.TrackRepository#getSong(de.rpgframework.music.UniqueTrackID)
	 */
	@Override
	public Track getSong(UniqueTrackID mbTrackID) {
		// TODO Auto-generated method stub
		return null;
	}

}
