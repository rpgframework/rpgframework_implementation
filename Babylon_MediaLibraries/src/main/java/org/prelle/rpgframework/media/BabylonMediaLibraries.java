/**
 * 
 */
package org.prelle.rpgframework.media;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.rpgframework.BabylonPlugin;
import org.prelle.rpgframework.ConfigContainerImpl;
import org.prelle.rpgframework.RPGFrameworkImpl;

import de.rpgframework.FunctionMediaLibraries;
import de.rpgframework.RPGFrameworkInitCallback;
import de.rpgframework.RPGFrameworkLoader.FunctionType;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.media.MediaService;

/**
 * @author prelle
 *
 */
public class BabylonMediaLibraries implements BabylonPlugin, FunctionMediaLibraries {

	protected Logger logger = LogManager.getLogger("babylon.media");

	private MediaServiceImpl mediaService;

	//-------------------------------------------------------------------
	public BabylonMediaLibraries() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.BabylonPlugin#getType()
	 */
	@Override
	public FunctionType getType() {
		return FunctionType.MEDIA_LIBRARIES;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.FunctionMediaLibraries#getMediaService()
	 */
	@Override
	public MediaService getMediaService() {
		return mediaService;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.BabylonPlugin#initialize(org.prelle.rpgframework.RPGFrameworkImpl, org.prelle.rpgframework.ConfigContainerImpl, de.rpgframework.RPGFrameworkInitCallback)
	 */
	@SuppressWarnings("exports")
	@Override
	public void initialize(RPGFrameworkImpl fwImpl, ConfigContainerImpl configRoot, RPGFrameworkInitCallback callback, List<RoleplayingSystem> limit) {
		logger.info("Initialize media libraries");
		try {
			mediaService     = new MediaServiceImpl(configRoot);
			fwImpl.setMediaLibraries(this);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

}
