/**
 * 
 */
package org.prelle.rpgframework.media.metadata;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author prelle
 *
 */
public class MetadataUtil {

	private final static Logger logger = LogManager.getLogger("babylon.media,meta");
	
	private static List<MetadataExtractor> extractor;
	
	//-------------------------------------------------------------------
	static {
		extractor = new ArrayList<>();
		
		extractor.add(new JPGMetadataExtractor());
		extractor.add(new PNGMetadataExtractor());
		extractor.add(new PDFMetadataExtractor());
		extractor.add(new JCodecVideoMetadataExtractor());
		extractor.add(new VideoMetadataExtractor());
		extractor.add(new CatchAllMetadataExtractor());
	}
	
	//-------------------------------------------------------------------
	public static byte[] getThumbnail(String filename, Path localFile) {
//		logger.debug("getThumbnail");
		String suffix = filename.toLowerCase().substring(filename.lastIndexOf(".")+1);
		// Check all extractors
		boolean triedExtractor = false;
		for (MetadataExtractor tool : extractor) {
			if (tool.getSupportedSuffixes().contains(suffix)) {
//				logger.debug("Use "+tool.getClass().getSimpleName()+" for "+filename);
				triedExtractor = true;
				byte[] ret = tool.readThumbnail(localFile);
				if (ret!=null)
					return ret;
			}
		}
		
		if (!triedExtractor)
			logger.warn("No metadata extractor for "+filename);
		else
			logger.warn("No metadata extractor worked for "+filename);
		return null;
	}
	
	//-------------------------------------------------------------------
	public static void writeThumbnail(byte[] data, Path imageFile) {
		String filename = imageFile.getFileName().toString().toLowerCase();
		String suffix = filename.substring(filename.lastIndexOf(".")+1);
		// Check all extractors
		for (MetadataExtractor tool : extractor) {
			if (tool.getSupportedSuffixes().contains(suffix)) {
				logger.debug("Use "+tool.getClass().getSimpleName()+" for "+filename);
				try {
					tool.writeThumbnail(data, imageFile);
				} catch (IOException e) {
					logger.error("Failed writing metadata for "+filename+" with "+tool.getClass().getSimpleName());
				}
				return;
			}
		}
		
		logger.warn("No metadata extractor to write "+filename);
	}
	
	//-------------------------------------------------------------------
	/**
	 * @param localFile May be null
	 */
	public static SerializableMetadata readMetadata(String filename, InputStream in, Path localFile) {
		String suffix = filename.toLowerCase().substring(filename.lastIndexOf(".")+1);
		logger.info("Read metadata from "+suffix+": "+filename);
		// Check all extractors
		for (MetadataExtractor tool : extractor) {
			if (tool.getSupportedSuffixes().contains(suffix)) {
				logger.debug("Use "+tool.getClass().getSimpleName()+" for "+filename);
				if (localFile!=null)
					return tool.readMetadata(localFile);
				else
					return tool.readMetadata(in);
//				logger.debug("Result of "+tool.getClass().getSimpleName()+": "+ ((new Gson()).toJson(tool.readMetadata(in, localFile))));
			}
		}
		
		logger.warn("No metadata extractor for "+filename);
		return new SerializableMetadata();
	}
	
	//-------------------------------------------------------------------
	public static void writeMetadata(SerializableMetadata meta, Path imageFile) {
		String filename = imageFile.getFileName().toString().toLowerCase();
		String suffix = filename.substring(filename.lastIndexOf(".")+1);
		// Check all extractors
		for (MetadataExtractor tool : extractor) {
			if (tool.getSupportedSuffixes().contains(suffix)) {
				logger.debug("Use "+tool.getClass().getSimpleName()+" for "+filename);
				try {
					tool.writeMetadata(meta, imageFile);
				} catch (IOException e) {
					logger.error("Failed writing metadata for "+filename+" with "+tool.getClass().getSimpleName());
				}
				return;
			}
		}
		
		logger.warn("No metadata extractor to write "+filename);
	}
	
	//-------------------------------------------------------------------
	static BufferedImage scale(BufferedImage imageToScale, int dWidth, int dHeight) {
        BufferedImage scaledImage = null;
        if (imageToScale != null) {
            scaledImage = new BufferedImage(dWidth, dHeight, imageToScale.getType());
            Graphics2D graphics2D = scaledImage.createGraphics();
            graphics2D.drawImage(imageToScale, 0, 0, dWidth, dHeight, null);
            graphics2D.dispose();
        }
        return scaledImage;
    }

	//-------------------------------------------------------------------
	static byte[] createThumbnailFromImage(InputStream in) throws IOException {
		if (in==null)
			throw new NullPointerException();
		/*
		 * Scale image to create thumbnail
		 */
		BufferedImage img = ImageIO.read(in);
		if (img==null)
			throw new IllegalArgumentException("No image in stream");
		double factX = 150.0 / img.getWidth(); 
		double factY = 150.0 / img.getHeight();
		double factor = Math.min(factX, factY);
		BufferedImage scaled = scale(img, (int)(img.getWidth()*factor), (int)(img.getHeight()*factor));
		BufferedImage newBufferedImage = new BufferedImage(scaled.getWidth(),
				scaled.getHeight(), BufferedImage.TYPE_INT_RGB);
		  newBufferedImage.createGraphics().drawImage(scaled, 0, 0, Color.WHITE, null);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write( newBufferedImage, "jpg", baos );
		baos.flush();
		byte[] imageInByte = baos.toByteArray();
		baos.close();
//		logger.debug("Return "+imageInByte.length+" bytes");
		return imageInByte;

	}

}
