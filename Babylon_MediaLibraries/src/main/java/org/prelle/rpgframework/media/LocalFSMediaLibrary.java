/**
 *
 */
package org.prelle.rpgframework.media;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.imageio.ImageIO;

import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.ImageWriteException;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.jpeg.JpegPhotoshopMetadata;
import org.apache.commons.imaging.formats.jpeg.exif.ExifRewriter;
import org.apache.commons.imaging.formats.tiff.TiffImageMetadata;
import org.apache.commons.imaging.formats.tiff.constants.ExifTagConstants;
import org.apache.commons.imaging.formats.tiff.constants.TiffTagConstants;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputDirectory;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputSet;
import org.apache.logging.log4j.LogManager;
import org.prelle.rpgframework.media.collector.DataCollector;
import org.prelle.rpgframework.media.collector.DataCollector.MediaURL;
import org.prelle.rpgframework.media.collector.DeviantArtDataCollector;
import org.prelle.rpgframework.media.collector.FlickrDataCollector;
import org.prelle.rpgframework.media.collector.ImgurDataCollector;
import org.prelle.rpgframework.media.collector.ImgurGalleryDataCollector;
import org.prelle.rpgframework.media.collector.LocalZIPFileDataCollector;
import org.prelle.rpgframework.media.collector.OpenGraphDataCollector;
import org.prelle.rpgframework.media.collector.PatreonCollector;
import org.prelle.rpgframework.media.map.BattleMapImpl;
import org.prelle.rpgframework.media.map.BattlemapSetImpl;
import org.prelle.rpgframework.media.map.TileDefinitionImpl;
import org.prelle.rpgframework.media.map.TilesetImpl;
import org.prelle.rpgframework.media.metadata.MetadataUtil;
import org.prelle.rpgframework.media.metadata.SerializableMetadata;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import de.rpgframework.core.EventBus;
import de.rpgframework.core.EventType;
import de.rpgframework.core.Genre;
import de.rpgframework.media.ImageMedia;
import de.rpgframework.media.LicenseType;
import de.rpgframework.media.Media;
import de.rpgframework.media.MediaCollection;
import de.rpgframework.media.MediaLibraryListener;
import de.rpgframework.media.MediaLibraryWaitCallback;
import de.rpgframework.media.MediaTag;
import de.rpgframework.media.RoleplayingMetadata.Category;
import de.rpgframework.media.TagRegistry;
import de.rpgframework.media.WritableMediaLibrary;
import de.rpgframework.media.map.Battlemap;
import de.rpgframework.media.map.TileDefinition;
import de.rpgframework.media.map.Tileset;
/**
 * @author prelle
 *
 */
public class LocalFSMediaLibrary extends AbstractMediaLibrary implements WritableMediaLibrary {

    @SuppressWarnings("exports")
	public class PathConverter implements JsonDeserializer<Path>, JsonSerializer<Path> {
		@Override
	    public Path deserialize(JsonElement JSONElement, Type type, JsonDeserializationContext JSONDeserializationContext) throws JsonParseException {
	        return Paths.get(JSONElement.getAsString());
	    }

	    @Override
	    public JsonElement serialize(Path path, Type type, JsonSerializationContext JSONSerializationContext) {
	        return new JsonPrimitive(path.toString());
	    }
	}

	private DataCollector[] preCollector = new DataCollector[] {
			new LocalZIPFileDataCollector(),
			new OpenGraphDataCollector(),
			new ImgurGalleryDataCollector(),
			new DeviantArtDataCollector(),
			new FlickrDataCollector(),
			new PatreonCollector(),
	};
	private DataCollector[] postCollector = new DataCollector[] {
			new ImgurDataCollector(),
			new DeviantArtDataCollector(),
			new FlickrDataCollector(),
	};
	private Path localBaseDir;
	private Gson gson;

	private PreparedStatement insert;
	private PreparedStatement update;
	private PreparedStatement delete;

	private List<MediaLibraryListener> listener;

	//-------------------------------------------------------------------
	public LocalFSMediaLibrary(LibraryDefinition libDef, Path localBaseDir) throws SQLException {
		super(libDef);
		this.localBaseDir = localBaseDir;
		listener = new ArrayList<MediaLibraryListener>();
		gson = new GsonBuilder().registerTypeHierarchyAdapter(Path.class, new PathConverter()).setPrettyPrinting().create();

		String id = libDef.getName().replace(' ', '_').replace('/', '_').replace('\\', '_');
		logger = LogManager.getLogger("babylon.media."+id);

		// Open it
		Path dbFile = localBaseDir.resolve("database.hsql");
		logger.debug("Expect database to be at "+dbFile);
		// If file does not exist yet, initialize database after connection is established
		Path checkFile = localBaseDir.resolve("database.hsql.log");
		boolean needsInit = !Files.exists(checkFile);

		con = DriverManager.getConnection("jdbc:hsqldb:file:"+dbFile, "SA", "");

		if (needsInit) {
			createDatabase();
		}


		Thread readAllData = new Thread( () -> openDatabase() );
		readAllData.start();

		/*
		 * Build string for Media Inserts
		 */
		List<String> head = new ArrayList<>();
		List<String> tail = new ArrayList<>();
		for (DatabaseDefinition def : DatabaseDefinition.values()) {
			head.add(def.columnName());
			tail.add("?");
		}
		insert = con.prepareStatement("INSERT INTO Media ("+String.join(",", head)+") VALUES ("+String.join(",", tail)+")");
		delete = con.prepareStatement("DELETE FROM Media WHERE uuid=?");
		// Remove UUID and path
		List<String> set = new ArrayList<>();
		for (DatabaseDefinition def : DatabaseDefinition.values())
			if (def!=DatabaseDefinition.UUID && def!=DatabaseDefinition.PATH) set.add(def.columnName()+"=?");
		try {
			update = con.prepareStatement("Update Media SET "+String.join(", ", set)+" WHERE "+DatabaseDefinition.UUID.columnName()+"=?");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(0);
		}
		// Update Genres
		Statement stat = con.createStatement();
		try {
			List<String> genreNames = new ArrayList<>();
			Arrays.asList(Genre.values()).forEach(g -> genreNames.add(g.name()));
			ResultSet rset = stat.executeQuery("SELECT genre FROM Genre");
			while (rset.next()) {
				genreNames.remove(rset.getString(1));
			}
			rset.close();
			for (String tmp : genreNames) {
				stat.executeUpdate("INSERT INTO Genre VALUES ('"+tmp+"')");
			}
			stat.close();
		} catch (SQLException e) {
		}
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "LocalFSMediaLib("+localBaseDir+")";
	}

	//-------------------------------------------------------------------
	protected void openDatabase() {
		super.openDatabase();
		for (AbstractMedia media : knownMedia.values()) {
			Path fullPath = localBaseDir.resolve(media.getRelativePath());
			try {
				media.setAccessURL(fullPath.toUri().toURL());
			} catch (MalformedURLException e) {
				logger.error("Error converting to URL: "+fullPath+" : "+e);
			}
		}
	}

	//-------------------------------------------------------------------
	private void createDatabase() {
		logger.warn("------Create database---------------");
		InputStream in = getClass().getResourceAsStream("babylon-media-database.sql");
		if (in==null)
			throw new NullPointerException("Cannot find database definition");
		BufferedReader reader = new BufferedReader(new InputStreamReader(in, Charset.forName("UTF-8")));
		StringBuffer buf = new StringBuffer();
		String line = null;
		Statement stat = null;
		try {
			while ( (line=reader.readLine())!=null) {
				if (line.contains("/*"))
					continue;
				if (line.isEmpty()) {
					stat = con.createStatement();
					String sql = buf.toString().trim();
					logger.debug("SEND: "+sql);
					stat.executeUpdate(sql);
					buf = new StringBuffer();
				} else
					buf.append(line);
			}
			String sql = buf.toString().trim();
			if (sql.length()>4) {
				logger.debug("SEND: "+sql);
				stat.executeUpdate(sql);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(0);
		} finally {
			try {
				stat.close();
			} catch (SQLException e) {
			}
		}
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	private void insertMedia(Media media) throws SQLException {
		if (media.getUUID()==null)
			throw new NullPointerException("Missing UUID in media");
		if (media.getRelativePath()==null)
			throw new NullPointerException("Missing relative path in media");

		if (knownMedia.containsKey(media.getUUID()))
			throw new IllegalStateException("Already know that UUID");
		knownMedia.put(media.getUUID(), (AbstractMedia)media);
		Path fullPath = localBaseDir.resolve(media.getRelativePath());
		try {
			((AbstractMedia)media).setAccessURL(fullPath.toUri().toURL());
		} catch (MalformedURLException e) {
			logger.error("Error converting to URL: "+fullPath+" : "+e);
		}

		insert.setString(DatabaseDefinition.UUID.columnPos(), media.getUUID().toString());
		insert.setString(DatabaseDefinition.PATH.columnPos(), media.getRelativePath().toString());
		insert.setString(DatabaseDefinition.TITLE.columnPos(), media.getName());
		insert.setString(DatabaseDefinition.ARTIST.columnPos(), media.getArtist());
		insert.setString(DatabaseDefinition.SERIES.columnPos(), media.getSeries());
		insert.setString(DatabaseDefinition.ORIGIN.columnPos(), (media.getSource()!=null)?media.getSource().toExternalForm():null);
		insert.setString(DatabaseDefinition.CATEGORY.columnPos(), (media.getCategory()!=null)?media.getCategory().name():null);
		insert.setString(DatabaseDefinition.LICENSE.columnPos(), (media.getLicense()!=null)?media.getLicense().name():null);
		insert.setString(DatabaseDefinition.COPYRIGHT.columnPos(), media.getCopyright());
		// Images
		if (media instanceof ImageMedia) {
			insert.setFloat(DatabaseDefinition.DPI.columnPos(), ((ImageMedia)media).getDPI());
		} else
			insert.setFloat(DatabaseDefinition.DPI.columnPos(), 0f);
		// Tiles
//		if (media instanceof TileDefinition) {
//			TileDefinition tile = (TileDefinition)media;
//			insert.setString(DatabaseDefinition.TILETYPE.columnPos(), tile.get);
//		} else {
			insert.setString(DatabaseDefinition.TILETYPE.columnPos(), null);
			insert.setBoolean(DatabaseDefinition.GEOMORPH.columnPos(), false);
//		}

		logger.debug("SQL: "+insert);
		insert.execute();

		/*
		 * Special handling for collection
		 */
		if (media instanceof MediaCollection) {
			writeMediaCollection((MediaCollection<? extends Media>) media);
		}
	}

	//-------------------------------------------------------------------
	private void writeMediaCollection(MediaCollection<? extends Media> parent) throws SQLException {
		Statement stat = null;
		try {
			stat = con.createStatement();
			/*
			 * Delete old collection info, if present
			 */
			stat.executeUpdate("DELETE FROM Collections WHERE parent='"+parent.getUUID()+"'");

			/*
			 * Now write current data
			 */
			for (Media child : parent) {
				stat.executeUpdate("INSERT INTO Collections (parent,child) VALUES('"+parent.getUUID()+"', '"+child.getUUID()+"')");
			}
		} finally {
			stat.close();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.WritableMediaLibrary#isReadOnly()
	 */
	@Override
	public boolean isReadOnly() {
		return !Files.isWritable(localBaseDir);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.WritableMediaLibrary#getCategories()
	 */
	@Override
	public Collection<Category> getCategories() {
		return Arrays.asList(Category.values());
//				Category.MAP, Category.NPC);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.WritableMediaLibrary#getMedia(de.rpgframework.media.Media.Category)
	 */
	@Override
	public List<Media> getMedia(Category value) {
		logger.info("getMedia("+value+")");

		//		Read all XML files in the directory first
		//		Then find media files and compare them to those stored inside the XML files
		//		For unassigned media files create empty XML files
		logger.warn("****************CONTINUE TO WORK HERE***************************");

		List<Media> ret = new ArrayList<>();

		/*
		 * Query database
		 */
		Statement stat = null;
		try {
			stat = con.createStatement();
			ResultSet set = stat.executeQuery("SELECT uuid FROM Media WHERE category='"+value+"'");
			while (set.next()) {
				UUID key = UUID.fromString(set.getString(1));
				Media media = knownMedia.get(key);
				if (media==null) {
					logger.warn("SQL database contains a UUID which is not cached yet: "+key);
				} else
					ret.add(media);
			}
			set.close();
			stat.close();
		} catch (SQLException e) {
			logger.error("Error querying database: "+e);
		}
//
//		/*
//		 * Search by genre
//		 */
//		for (Genre genre : Genre.values()) {
//			Path dirToCheck = localBaseDir.resolve(genre.name().toLowerCase()).resolve(value.name().toLowerCase());
//			if (Files.notExists(dirToCheck)) {
//				logger.debug("No directory "+dirToCheck);
//				continue;
//			}
//			logger.debug("Add files from "+dirToCheck);
//
//			try {
//				for (Path node : Files.newDirectoryStream(dirToCheck)) {
//					if (Files.isRegularFile(node)) {
//						logger.debug("* media "+node);
//						ImageMediaImpl media = new ImageMediaImpl(node, value);
//						media.addGenre(genre);
//						if (FullRPGFrameworkLoader.getInstance().getWebserver()!=null) {
//							URL url = FullRPGFrameworkLoader.getInstance().getWebserver().toURL(node);
//							logger.debug("  "+url);
//							media.setSource(url);
//						}
//						ret.add(media);
//
//						// For now, ignore non JPEG
//						if (!node.getFileName().toString().endsWith(".jpg"))
//							continue;
//
////						// Dump meta
////						readIPTCData(media);
////						try {
////							ImageMetadata metadata = Imaging.getMetadata(node.toFile());
////							if (metadata!=null) {
////								List<? extends ImageMetadataItem> items = metadata.getItems();
////								for (int i = 0; i < items.size(); i++) {
////									final ImageMetadataItem item = items.get(i);
////									logger.debug("    " + "item: " + item);
////								}
////
////								if (metadata instanceof JpegImageMetadata) {
////									JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;
////									printTagValue(jpegMetadata, TiffTagConstants.TIFF_TAG_XRESOLUTION);
////									printTagValue(jpegMetadata, TiffTagConstants.TIFF_TAG_DATE_TIME);
////									JpegPhotoshopMetadata itpc = jpegMetadata.getPhotoshop();
////									if (itpc!=null) {
////										for (ImageMetadataItem field : itpc.getItems()) {
////											logger.debug("    ..."+field);
////										}
////									}
////								}
////							} else
////								logger.debug("  NO METADATA");
////						} catch (ImageReadException e) {
////							// TODO Auto-generated catch block
////							e.printStackTrace();
////						}
////						//----------------------------
////						try {
////							System.err.println(media.getPath().getFileName().toString());
////							if (media.getPath().getFileName().toString().equals("twisted_branch_lair__day__by_hero339-d99yaoq.jpg")) {
////								updateTag(media);
////								writeIPTCData(media);
////							}
////						} catch (ImageReadException | ImageWriteException e1) {
////							// TODO Auto-generated catch block
////							e1.printStackTrace();
////						}
//					}
//				}
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}

		/*
		 * Search by roleplaying system
		 */
//		for (RoleplayingSystem rules : RoleplayingSystem.values()) {
//			Path dirToCheck = localBaseDir.resolve(rules.name().toLowerCase()).resolve(value.name().toLowerCase());
//			if (Files.notExists(dirToCheck)) {
//				logger.debug("No directory "+dirToCheck);
//				continue;
//			}
//			logger.debug("Add files from "+dirToCheck);
//
//			try {
//				for (Path node : Files.newDirectoryStream(dirToCheck)) {
//					if (Files.isRegularFile(node)) {
//						logger.debug("* media "+node);
//						ImageMediaImpl media = new ImageMediaImpl(node, value);
//						for (Genre genre : rules.getGenre())
//							media.addGenre(genre);
//						if (FullRPGFrameworkLoader.getInstance().getWebserver()!=null) {
//							URL url = FullRPGFrameworkLoader.getInstance().getWebserver().toURL(node);
//							logger.debug("  "+url);
//							media.setSource(url);
//						}
//						//						URL url = new URL("http", "192.168.0.2", 6789, "/"+localBaseDir.relativize(node).toString());
//						//						logger.debug("  "+url);
//						//						media.setSource(url);
//						ret.add(media);
//					}
//				}
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}


		return ret;
	}

	//-------------------------------------------------------------------
	private void updateTag(ImageMediaImpl media) throws IOException, ImageReadException, ImageWriteException {
		File jpegImageFile = media.getRelativePath().toFile();

		File dst = new File(media.getRelativePath()+".jpg");
		try (
				FileOutputStream fos = new FileOutputStream(dst);
				OutputStream os = new BufferedOutputStream(fos);
				) {

			TiffOutputSet outputSet = null;
			JpegPhotoshopMetadata iptc = null;

			// note that metadata might be null if no metadata is found.
			final ImageMetadata metadata = Imaging.getMetadata(jpegImageFile);
			final JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;
			if (null != jpegMetadata) {
				// note that exif might be null if no Exif metadata is found.
				final TiffImageMetadata exif = jpegMetadata.getExif();

				if (null != exif) {
					// TiffImageMetadata class is immutable (read-only).
					// TiffOutputSet class represents the Exif data to write.
					//
					// Usually, we want to update existing Exif metadata by
					// changing
					// the values of a few fields, or adding a field.
					// In these cases, it is easiest to use getOutputSet() to
					// start with a "copy" of the fields read from the image.
					outputSet = exif.getOutputSet();
				}
				iptc = jpegMetadata.getPhotoshop();
				logger.debug("iptc = "+iptc);
			}

			// if file does not contain any exif metadata, we create an empty
			// set of exif metadata. Otherwise, we keep all of the other
			// existing tags.
			if (null == outputSet) {
				outputSet = new TiffOutputSet();
//				outputSet.findField();
//				iptc = jpegMetadata.getPhotoshop();
//				logger.debug("iptc = "+iptc);
			}

			{
				// Example of how to add a field/tag to the output set.
				//
				// Note that you should first remove the field/tag if it already
				// exists in this directory, or you may end up with duplicate
				// tags. See above.
				//
				// Certain fields/tags are expected in certain Exif directories;
				// Others can occur in more than one directory (and often have a
				// different meaning in different directories).
				//
				// TagInfo constants often contain a description of what
				// directories are associated with a given tag.
				//
				final TiffOutputDirectory exifDirectory = outputSet.getOrCreateExifDirectory();
				// make sure to remove old value if present (this method will
				// not fail if the tag does not exist).
				exifDirectory.removeField(ExifTagConstants.EXIF_TAG_OWNER_NAME);
				exifDirectory.add(ExifTagConstants.EXIF_TAG_OWNER_NAME, "Hallo Welt");
				exifDirectory.removeField(ExifTagConstants.EXIF_TAG_USER_COMMENT);
				exifDirectory.add(ExifTagConstants.EXIF_TAG_USER_COMMENT, Arrays.asList(media.getGenres()).toString());
				exifDirectory.removeField(TiffTagConstants.TIFF_TAG_DOCUMENT_NAME);
				exifDirectory.add(TiffTagConstants.TIFF_TAG_DOCUMENT_NAME, "Dies ist der Document Name");
			}

//			if (itpc!=null) {
//				TiffMetadataItem item = new TiffMetadataItem(new Tiff);
//				itpc.add();
//			}

			// printTagValue(jpegMetadata, TiffConstants.TIFF_TAG_DATE_TIME);

			new ExifRewriter().updateExifMetadataLossless(jpegImageFile, os,
					outputSet);
			logger.debug("Wrote "+dst.getAbsolutePath());
//			logger.fatal("Stop here");
//			System.exit(0);
		}
	}

//	//-------------------------------------------------------------------
//	private void readIPTCData(ImageMediaImpl media) {
//		logger.trace("START readIPTCData");
//		try {
//			File jpegImageFile = media.getPath().toFile();
//			if (jpegImageFile==null) return;
//
//			ImageMetadata abstractMeta = Imaging.getMetadata(jpegImageFile);
//			if (abstractMeta==null || !(abstractMeta instanceof JpegImageMetadata))
//				return;
//			JpegImageMetadata metadata = (JpegImageMetadata)abstractMeta;
//
//			JpegPhotoshopMetadata psMetadata = metadata.getPhotoshop();
//			if (psMetadata==null) return;
//			psMetadata.dump();
//	        List<IptcRecord> oldRecords = psMetadata.photoshopApp13Data.getRecords();
//
//	        for (final IptcRecord record : oldRecords) {
//	            if (record.iptcType != IptcTypes.CITY) {
//	                logger.debug("Key: " + record.iptcType.getName() + " (0x"
//	                        + Integer.toHexString(record.iptcType.getType())
//	                        + "), value: " + record.getValue());
//	            }
//	        }
//		} catch (Exception e) {
//			logger.error("FAIL  readIPTCData",e);
//		} finally {
//			logger.trace("STOP  readIPTCData");
//		}
//	}
//
//	//-------------------------------------------------------------------
//	private void writeIPTCData(ImageMediaImpl media) {
//		logger.trace("START readIPTCData");
//		try {
//			File jpegImageFile = media.getPath().toFile();
//			if (jpegImageFile==null) return;
//
//			ImageMetadata abstractMeta = Imaging.getMetadata(jpegImageFile);
//			if (abstractMeta==null || !(abstractMeta instanceof JpegImageMetadata))
//				return;
//			JpegImageMetadata jpegMetadata = (JpegImageMetadata)abstractMeta;
//			if (jpegMetadata==null) return;
//
//			/*
//			 * Remove those tags we intend to overwrite, keep others
//			 */
//			List<IptcBlock> newBlocks = new ArrayList<>();
//			List<IptcRecord> newRecords = new ArrayList<>();
//			/*
//			 * If there is an old header, fill the old blocks except with
//			 * those we want to overwrite
//			 */
//			JpegPhotoshopMetadata metadata = jpegMetadata.getPhotoshop();
//			if (metadata!=null) {
//		        newBlocks.addAll(metadata.photoshopApp13Data.getNonIptcBlocks());
//		        for (IptcRecord record : metadata.photoshopApp13Data.getRecords()) {
//		            if (record.iptcType != IptcTypes.CITY
//		                    && record.iptcType != IptcTypes.CREDIT) {
//		                newRecords.add(record);
//		            }
//		        }
//			}
//
//			// Write new data
//	        newRecords.add(new IptcRecord(IptcTypes.CITY, "Albany, NY"));
//	        newRecords.add(new IptcRecord(IptcTypes.CREDIT, "William Sorensen"));
//
//	        PhotoshopApp13Data newData = new PhotoshopApp13Data(newRecords, newBlocks);
//	        ByteSource byteSource = new ByteSourceFile(jpegImageFile);
//	        final File updated = new File(jpegImageFile.getAbsolutePath()+".jpg");
//	        try (FileOutputStream fos = new FileOutputStream(updated);
//	                OutputStream os = new BufferedOutputStream(fos)) {
//	            new JpegIptcRewriter().writeIPTC(byteSource, os, newData);
//	        }
//
//	        final ByteSource updateByteSource = new ByteSourceFile(updated);
//	        final JpegPhotoshopMetadata outMetadata = new JpegImageParser().getPhotoshopMetadata(
//	                updateByteSource, new HashMap<>());
//
////	        try (
////	        		FileOutputStream fos = new FileOutputStream(jpegImageFile);
////	                OutputStream os = new BufferedOutputStream(fos)) {
////	            new JpegIptcRewriter().writeIPTC(byteSource, os, newData);
////	        }
////
////	        JpegPhotoshopMetadata outMetadata = new JpegImageParser().getPhotoshopMetadata(
////	        		byteSource, new HashMap<>());
// 		} catch (Exception e) {
//			logger.error("FAIL  readIPTCData",e);
//		} finally {
//			logger.trace("STOP  readIPTCData");
//		}
//	}

	//-------------------------------------------------------------------
	public Path getBaseDirectory() {
		return localBaseDir;
	}

	//-------------------------------------------------------------------
	private static String toCamelCase(String val) {
		StringBuffer buf = new StringBuffer();
		boolean makeUpper = true;
		for (int i=0; i<val.length(); i++) {
			char c = val.charAt(i);
			if (Character.isWhitespace(c) || c=='_')
				makeUpper= true;
			else {
				if (makeUpper) {
					c = Character.toUpperCase(c);
				}
				makeUpper = false;
				buf.append(c);
			}
		}
		return buf.toString();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.WritableMediaLibrary#createTileset(java.lang.String)
	 */
	@Override
	public Tileset createTileset(String title, String series, String artist, URL sourceURL) throws IOException {
		logger.debug(String.format("START: createTileset(title=%s, series=%s, artist=%s, srcURL=%s)", title, series, artist, sourceURL));
		try {
			if (title==null)
				throw new IOException("Title must be set");
			List<String> elements = new ArrayList<>();
			if (artist!=null) elements.add(toCamelCase(artist));
			if (series!=null) elements.add(toCamelCase(series));
			elements.add(toCamelCase(title));
			String directoryName = String.join("_", elements);

			Path newDirectory = localBaseDir.resolve("tileset").resolve(directoryName);
			if (Files.exists(newDirectory))
				throw new IOException("Tileset with that name already exists in this library");

			logger.info("1. Create directory "+newDirectory.toAbsolutePath());
			newDirectory = Files.createDirectories(newDirectory);
			TilesetImpl ret = new TilesetImpl(newDirectory);
			ret.setLibrary(this);
			ret.setUUID(UUID.randomUUID());
			ret.setArtist(artist);
			ret.setSeries(series);
			ret.setSource(sourceURL);
			ret.setName(title);
			ret.setLicense(LicenseType.UNKNOWN);

			/*
			 * Create a info structure
			 */
			logger.info("2. Store tileset metadata to "+newDirectory.toAbsolutePath());
			Path infoFile = newDirectory.resolve("tileset.json");
			// Store relative path in media
			try {
				ret.setRelativePath(localBaseDir.relativize(infoFile));
				String json = gson.toJson(ret);
				BufferedWriter out = Files.newBufferedWriter(infoFile, StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE);
				out.write(json);
				out.close();
				logger.debug("   Wrote "+infoFile);
			} catch (Throwable e) {
				StringWriter out = new StringWriter();
				e.printStackTrace(new PrintWriter(out));
				logger.error("Failed to write JSON file to "+infoFile+": "+e);
				logger.error("\n"+out.toString().substring(0, 1200));
				System.exit(0);
			}

			/*
			 * Add to SQL
			 */
			try {
				logger.info("3. Store tileset metadata in SQL");
				insertMedia(ret);
			} catch (SQLException e) {
				logger.error("Failed adding media to database: "+e);
				System.exit(0);
			}

			return ret;
		} finally {
			logger.debug("STOP : createTileset()");
		}

	}

	//-------------------------------------------------------------------
	public void internalAddMedia(AbstractMedia media) {
		logger.debug("Fake add media "+media);
		media.setLibrary(this);
		try {
			logger.info("3. Store metadata in SQL database");
			insertMedia(media);
		} catch (SQLException e) {
			logger.error("Failed adding media to database: "+e,e);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.WritableMediaLibrary#createTile(de.rpgframework.media.map.Tileset, java.lang.String, java.lang.String, java.lang.String, java.net.URL, java.nio.file.Path)
	 */
	public TileDefinition createTile(Tileset set, String title, String series, String artist, URL sourceURL, Path imageFile) throws IOException {
		logger.debug(String.format("START: createTile(title=%s, series=%s, artist=%s, srcURL=%s, file=%s)", title, series, artist, sourceURL, imageFile));
		try {
			logger.info("1. Add information from image file itself");
			SerializableMetadata meta = MetadataUtil.readMetadata(imageFile.getFileName().toString(), new FileInputStream(imageFile.toFile()), imageFile);
			if (meta==null) {
				logger.fatal("No metadata");
				System.exit(0);
			}

			logger.info("2. Create tile "+imageFile+" in set "+set);
			TileDefinitionImpl image = new TileDefinitionImpl(meta);
			image.setLibrary(this);
			if (image.getUUID()==null)
				image.setUUID(UUID.randomUUID());
			image.setArtist(artist);
			image.setName(title);
			image.setSeries(series);
			image.setSource(sourceURL);
			image.setRelativePath(localBaseDir.relativize(imageFile));
			image.setLicense(LicenseType.UNKNOWN);

//			MediaHelper.populateMetadataFromFile(new FileInputStream(imageFile.toFile()), imageFile.getFileName().toString(), image);
//			// If no thumbnail set yet, create it
//			if (image.getThumbnail()==null) {
//				image.setThumbnail(MediaHelper.scaleAsThumbnail(new FileInputStream(imageFile.toFile())));
//			}

			/*
			 * Add to SQL
			 */
			try {
				logger.info("3. Store metadata in SQL database");
				insertMedia(image);
			} catch (SQLException e) {
				logger.error("Failed adding media to database: "+e,e);
			}

			/*
			 * Write metadata to file
			 */
			logger.info("4. Store metadata in file itself (if possible)");
			MediaHelper.writeMetadataToFile(image, imageFile);

			EventBus.fireEvent(this, EventType.MEDIA_ADDED, this, image);
			return image;
		} finally {
			logger.debug("STOP : createTile()");
		}
	}

	//-------------------------------------------------------------------
	/**
	 *    For http(s)-URLs, generate a file in a subdirectory and load
	 *    URL content into it.
	 *    For file-URLs that are outside the library directory, generate
	 *    a file and copy file content to it.
	 *    For file-URLs that already are inside the library directory,
	 *    check if file is already known, otherwise create a new media
	 *    for it
	 */
	private Path copyNonCollectionURLToLibrary(LocalFSMediaLibrary library, Category category, MediaURL murl) throws IOException {
		logger.debug("  START: copyNonCollectionURLToLibrary("+category+", "+murl+")");

		try {
			// Detect filename from URL
			String filename = murl.filename;

			// Create a default path to copy content to
			Path target = library.getBaseDirectory().resolve(category.name().toLowerCase()).resolve(filename);
			logger.debug("    target = "+target.toAbsolutePath());
			// Eventually create parent dir
			if (!Files.exists(target.getParent()))
				Files.createDirectories(target.getParent());

			// Copy
			if (murl.url.getProtocol().startsWith("http")) {
				// Download URL content to file
				HttpURLConnection con = (HttpURLConnection) murl.url.openConnection();
//				con.addRequestProperty("Host", "www.deviantart.com");
				con.addRequestProperty("User-Agent", "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:54.0) Gecko/20100101 Firefox/54.0");
				int code = con.getResponseCode();
				logger.debug("    code is "+code);
				if (code!=200) {
					logger.error("    Code "+code);
					throw new IOException("Http error code "+code+" when accessing "+murl);
				}
				Files.copy(con.getInputStream(), target);
				logger.debug("    downloaded file to "+target+"  ("+Files.size(target)+" bytes)");
			} else if (murl.url.getProtocol().startsWith("file")) {
				// Check if source file is inside or outside library directory
				try {
					Path orig =  (new File(murl.url.toURI())).toPath();
					if (orig.startsWith(library.getBaseDirectory())) {
						// Inside base directory
						logger.debug("    source file is inside library");
						target = orig;
					} else {
						// Outside base directory
						logger.debug("    source file is outside library: "+orig.toFile());
						Files.copy(new FileInputStream(orig.toFile()), target);
						logger.debug("    copied file to "+target);
					}
				} catch (URISyntaxException e) {
					logger.error("Error converting URL to file: "+e);
					throw new IOException("Error converting URL to file", e);
				}
			}

			return target;
		} finally {
			logger.debug("  STOP : copyNonCollectionURLToLibrary("+category+", "+murl+")");
		}
	}

	//-------------------------------------------------------------------
	/**
	 *    For http(s)-URLs, generate a file in a subdirectory and load
	 *    URL content into it.
	 *    For file-URLs that are outside the library directory, generate
	 *    a file and copy file content to it.
	 *    For file-URLs that already are inside the library directory,
	 *    check if file is already known, otherwise create a new media
	 *    for it
	 */
//	private Path copyCollectionURLToLibrary(LocalFSMediaLibrary library, Category category, URL url) throws IOException {
//		logger.debug("  START: copyCollectionURLToLibrary("+category+", "+url+")");
//
//		try {
//			// Detect filename from URL
//			String filename = url.getPath();
//			if (filename.lastIndexOf('/')>0)
//				filename = filename.substring(filename.lastIndexOf('/')+1);
//			// For collection URLs, remove suffix
//			if (filename.lastIndexOf('.')>0)
//				filename = filename.substring(0,filename.lastIndexOf('.'));
//			logger.debug("    filename = "+filename);
//
//			// Create a default path to copy content to
//			Path target = library.getBaseDirectory().resolve(category.name().toLowerCase()).resolve(filename);
//			logger.debug("    target = "+target.toAbsolutePath());
//
//			// Copy
//			if (url.getProtocol().startsWith("http")) {
//				// Download URL content to file
//				HttpURLConnection con = (HttpURLConnection) url.openConnection();
//				int code = con.getResponseCode();
//				if (code!=200)
//					throw new IOException("Http error code "+code+" when accessing "+url);
//				Files.copy(con.getInputStream(), target);
//				logger.debug("    downloaded file to "+target);
//			} else if (url.getProtocol().startsWith("file")) {
//				// Check if source file is inside or outside library directory
//				try {
//					Path orig =  (new File(url.toURI())).toPath();
//					if (orig.startsWith(library.getBaseDirectory())) {
//						// Inside base directory
//						logger.debug("    source file is inside library");
//						target = orig;
//					} else {
//						// Outside base directory
//						logger.debug("    source file is outside library");
//						Files.copy(new FileInputStream(orig.toFile()), target);
//						logger.debug("    copied file to "+target);
//					}
//				} catch (URISyntaxException e) {
//					logger.error("Error converting URL to file: "+e);
//					throw new IOException("Error converting URL to file", e);
//				}
//			}
//
//			return target;
//		} finally {
//			logger.debug("  STOP : copyCollectionURLToLibrary("+category+", "+url+")");
//		}
//	}

	//-------------------------------------------------------------------
	private Media importFromStream(InputStream in, Path dstFile, Category category) throws IOException {
		if (Files.exists(dstFile))
			throw new IOException("Target file already exists: "+dstFile.toAbsolutePath());
		logger.info("Copy to "+dstFile);
		// copy stream data to local file
		Files.copy(in, dstFile);

		Media ret = MediaHelper.createMediaFromFile(dstFile);
		ret.setCategory(category);

		return ret;
	}

	//-------------------------------------------------------------------
	public boolean canBeImported(String fileSuffix) {
		String test = fileSuffix.toLowerCase();
		switch (test) {
		case "jpg": case "png": case "tif":
		case "pdf":
		case "m4v":
			return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.WritableMediaLibrary#importFromFilesystem(java.io.File, de.rpgframework.media.RoleplayingMetadata.Category)
	 */
	@Override
	public Media importFromFilesystem(File srcFile, Category category) throws IOException {
		logger.debug("START: importFromFileSystem("+srcFile+", "+category+")");
		try {
			InputStream src = new FileInputStream(srcFile);
			Path dstFile = localBaseDir.resolve(srcFile.getName());

			return importFromStream(src, dstFile, category);
		} finally {
			logger.debug("STOP : importFromFileSystem("+srcFile+", "+category+")");
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.WritableMediaLibrary#importFromFilesystem(java.io.File, de.rpgframework.media.MediaCollection)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T extends Media> T importFromFilesystem(File srcFile, MediaCollection<T> context) throws IOException {
		Category category = context.getCategory();
		InputStream src = new FileInputStream(srcFile);
		Path dstFile = context.getLocalPath().resolve(srcFile.getName());
		return (T) importFromStream(src, dstFile, category);
	}

	//-------------------------------------------------------------------
	private Media importMediaFromGroup(DataCollector.MediaURL mediaUrl, Category category, SerializableMetadata groupData) throws IOException {
		URL url = mediaUrl.url;
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		// Is file accessible
		if (con.getResponseCode()==404)
			throw new FileNotFoundException("No data at "+url);
		if (con.getResponseCode()==401)
			throw new FileNotFoundException("Credentials missing for "+url);
		if (con.getResponseCode()!=200)
			throw new FileNotFoundException("Cannot load from "+url+"  : "+con.getResponseCode()+" "+con.getResponseMessage());

		String filename = (mediaUrl.filename!=null)?mediaUrl.filename:url.getFile();
		Path dstFile = localBaseDir.resolve(Paths.get(filename).getFileName());
		Media media = importFromStream(con.getInputStream(), dstFile, category);
		media.setSource(url);

		return media;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.WritableMediaLibrary#importFromWebserver(java.net.URL, de.rpgframework.media.RoleplayingMetadata.Category)
	 */
	@Override
	public List<Media> importFromWebserver(URL url, Category category) throws IOException {
		logger.info("START: Import "+category+" from "+url);
		List<Media> ret = new ArrayList<>();

		try {
			if (!url.getProtocol().startsWith("http"))
				throw new IllegalArgumentException("Only http/https URLs supported");

			/*
			 * Check for special site hooks
			 */
			SerializableMetadata preMeta = new SerializableMetadata();
			logger.debug("1. Prefill phase");
			List<MediaURL> mediaURLs = new ArrayList<>();
			for (DataCollector col : preCollector) {
				if (col.matches(url)) {
					logger.debug("  call prefill on "+col.getClass().getSimpleName());
					mediaURLs.addAll( col.preFillMetadata(url, preMeta) );
				}
			}

			logger.debug("2. Fill from medias itself using "+url);
			for (MediaURL mUrl : mediaURLs) {
				logger.debug("2. Fill from medias itself using "+mUrl.url);

				Media media = importMediaFromGroup(mUrl, category, preMeta);

				/*
				 * Check for special site hooks
				 */
				logger.debug("3. Post fill");
				for (DataCollector col : postCollector) {
					if (col.matches(url))
						col.fillMetadata(url, media);
				}

				// Check to apply pre fill data
				if (preMeta.getTitle()!=null && media.getName()==null) {
					logger.debug("  Set media name to "+preMeta.getTitle());
					media.setName(preMeta.getTitle());
				}

				logger.info("Found "+media);
				ret.add(media);
			}

		} finally {
			logger.info("STOP : Imported "+ret);
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.WritableMediaLibrary#importFromWebserver(java.net.URL, de.rpgframework.media.MediaCollection)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T extends Media> T importFromWebserver(URL url, MediaCollection<T> context) throws IOException {
		if (!url.getProtocol().startsWith("http"))
			throw new IllegalArgumentException("Only http/https URLs supported");

		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		// Is file accessible
		if (con.getResponseCode()==404)
			throw new FileNotFoundException("No data at "+url);
		if (con.getResponseCode()==401)
			throw new FileNotFoundException("Credentials missing for "+url);
		if (con.getResponseCode()!=200)
			throw new FileNotFoundException("Cannot load from "+url+"  : "+con.getResponseCode()+" "+con.getResponseMessage());

		Category category = context.getCategory();
		Path dstFile = context.getLocalPath().resolve(url.getFile());
		return (T) importFromStream(con.getInputStream(), dstFile, category);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.WritableMediaLibrary#getBytes(de.rpgframework.media.Media)
	 */
	@Override
	public byte[] getBytes(Media media) {
		Path realPath = localBaseDir.resolve(media.getRelativePath());
		try {
			return Files.readAllBytes(realPath);
		} catch (IOException e) {
			logger.error("Failed accessing "+realPath,e);
			return null;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.WritableMediaLibrary#save(de.rpgframework.media.Media)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void save(Media media) throws IOException {
		logger.debug("START: save()");
		try {
		if (media.getUUID()==null)
			throw new NullPointerException("Missing UUID in media");
		if (media.getRelativePath()==null)
			throw new NullPointerException("Missing relative path in media");
		if ( !media.isDirty() ) {
			logger.warn("Did not save - media is not dirty: "+media.getName());
			return;
		}

		try {
			update.setString(DatabaseDefinition.TITLE.columnPos()-2, media.getName());
			update.setString(DatabaseDefinition.ARTIST.columnPos()-2, media.getArtist());
			update.setString(DatabaseDefinition.SERIES.columnPos()-2, media.getSeries());
			update.setString(DatabaseDefinition.ORIGIN.columnPos()-2, (media.getSource()!=null)?media.getSource().toExternalForm():null);
			update.setString(DatabaseDefinition.CATEGORY.columnPos()-2, (media.getCategory()!=null)?media.getCategory().name():null);
			update.setString(DatabaseDefinition.LICENSE.columnPos()-2, (media.getLicense()!=null)?media.getLicense().name():null);
			update.setString(DatabaseDefinition.COPYRIGHT.columnPos()-2, media.getCopyright());
			// Images
			if (media instanceof ImageMedia) {
				update.setFloat(DatabaseDefinition.DPI.columnPos()-2, ((ImageMedia)media).getDPI());
			} else
				update.setFloat(DatabaseDefinition.DPI.columnPos()-2, 0f);
			// Tiles
//		if (media instanceof TileDefinition) {
//			TileDefinition tile = (TileDefinition)media;
//			insert.setString(DatabaseDefinition.TILETYPE.columnPos(), tile.get);
//		} else {
			update.setString(DatabaseDefinition.TILETYPE.columnPos()-2, null);
			update.setBoolean(DatabaseDefinition.GEOMORPH.columnPos()-2, false);
//		}

			// Last parameter is the UUID again
			update.setString(DatabaseDefinition.values().length-1, media.getUUID().toString());

			logger.debug("SQL: "+update);
			update.execute();

			/*
			 * Update tags
			 */
			Statement stat = con.createStatement();
			stat.executeUpdate("DELETE FROM MediaTags WHERE media='"+media.getUUID()+"'");
			// Add tags
			for (MediaTag tag : media.getTags()) {
				stat.addBatch("INSERT INTO MediaTags (media,type,value) VALUES ('"+media.getUUID()+"','"+TagRegistry.getTagTypeID(tag)+"','"+tag.name()+"')");
			}
			stat.executeBatch();
			// Add genres
			stat.executeUpdate("DELETE FROM MediaGenre WHERE media='"+media.getUUID()+"'");
			for (Genre genre : media.getGenres()) {
				stat.addBatch("INSERT INTO MediaGenre (media,genre) VALUES ('"+media.getUUID()+"','"+genre.name()+"')");
			}
			stat.executeBatch();
			// Add keywords
			stat.executeUpdate("DELETE FROM MediaKeywords WHERE media='"+media.getUUID()+"'");
			for (String keyword : media.getKeywords()) {
				stat.addBatch("INSERT INTO MediaKeywords (media,word) VALUES ('"+media.getUUID()+"','"+keyword+"')");
			}
			stat.executeBatch();
			stat.close();

			/*
			 * Special handling for collection
			 */
			if (media instanceof MediaCollection) {
				writeMediaCollection((MediaCollection<? extends Media>) media);
			}


			// Fire events
			logger.debug("7b. Fire events-----------------------------------------");
			for (MediaLibraryListener callback : listener) {
				try {
					callback.mediaUpdated(media);
				} catch (Exception e) {
					logger.error("Error delivering media event");
				}
			}
			EventBus.fireEvent(this, EventType.MEDIA_UPDATED, this, media);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/*
		 * Write to media itself
		 */
		logger.debug("Update file "+media.getRelativePath());
		MediaHelper.writeMetadataToFile(media, localBaseDir.resolve(media.getRelativePath()));
		} finally {
			logger.debug("STOP : save()");
		}
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see org.prelle.rpgframework.media.AbstractMediaLibrary#getThumbnail(java.nio.file.Path)
//	 */
//	@Override
//	protected byte[] getThumbnail(Path relativePath) {
//		Path absPath = localBaseDir.resolve(relativePath);
//		logger.debug("getThumbnail for "+absPath);
//		try {
//			byte[] ret = MediaHelper.readThumbnail(new FileInputStream(absPath.toFile()));
//			if (ret==null)
//				ret = MediaHelper.scaleAsThumbnail(new FileInputStream(absPath.toFile()));
//			return ret;
//		} catch (Exception e) {
//			logger.error("Error obtaining thumbnail from "+relativePath+": "+e,e);
//		}
//		return null;
//	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.WritableMediaLibrary#getInputStream(de.rpgframework.media.Media)
	 */
	@Override
	public InputStream getInputStream(Media media) {
		Path absPath = localBaseDir.resolve(media.getRelativePath());
		try {
			return new FileInputStream(absPath.toFile());
		} catch (FileNotFoundException e) {
			logger.error("No such file: "+absPath);
			return null;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.MediaLibrary#getLocalPath(de.rpgframework.media.Media)
	 */
	@Override
	public Path getLocalPath(Media media) {
		return localBaseDir.resolve(media.getRelativePath());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.WritableMediaLibrary#addTilesFromZIP(de.rpgframework.media.map.Tileset, java.io.File, de.rpgframework.media.MediaLibraryWaitCallback)
	 */
	@Override
	public void addTilesFromZIP(Tileset set, File file, MediaLibraryWaitCallback callback) {
		logger.debug("START: addTilesFromZIP "+file+" to "+set.getLocalPath());

		Thread importThread = new Thread(new Runnable() {
			public void run() {
				try {
					ZipFile zip = new ZipFile(file);
					// Count images
					int sum = 0;
					Enumeration<? extends ZipEntry> e = zip.entries();
					while (e.hasMoreElements()) {
						ZipEntry entry = e.nextElement();
						String filename = entry.getName();
						if (filename.toLowerCase().endsWith(".jpg") || filename.toLowerCase().endsWith(".png"))
							sum++;
					}

					callback.percentChanged(0);
					// Now process
					e = zip.entries();
					int count=0;
					while (e.hasMoreElements()) {
						ZipEntry entry = e.nextElement();
						String filename = entry.getName();
						if (filename.contains("/"))
							filename = filename.substring(filename.indexOf("/")+1);
						logger.debug("* "+entry);
						if (filename.toLowerCase().endsWith(".jpg") || filename.toLowerCase().endsWith(".png")) {
							// Copy file
							Path target = set.getLocalPath().resolve(filename);
							logger.info("Unpack "+entry.getName()+" to "+target);
							Files.copy(zip.getInputStream(entry), target, StandardCopyOption.REPLACE_EXISTING);
							logger.debug("create in library");
							TileDefinition tile = createTile(set, filename.substring(0, filename.lastIndexOf(".")), set.getSeries(), set.getArtist(), set.getSource(), target);
							set.add(tile);
						}
						count++;
						callback.percentChanged( ((double)count)/((double)sum));
					}
					zip.close();

					// Update tileset
					logger.debug("Save updated tileset");
					save(set);

					callback.finished();
				} catch (IOException e) {
					logger.error("Failed unpacking ZIP",e);
				} finally {
					logger.debug("STOP: addTilesFromZIP");
				}
			}}, "ImportThread");
		importThread.start();
	}

	//-------------------------------------------------------------------
	/**
	 * @throws IOException
	 * @see de.rpgframework.media.MediaService#importURLToLibrary(de.rpgframework.media.WritableMediaLibrary, de.rpgframework.media.RoleplayingMetadata.Category, java.net.URL)
	 */
	@Override
	public List<Media> importURL(Category category, URL url) throws IOException {
		logger.debug("START: importURL("+category+", "+url+")");
		List<Media> ret = new ArrayList<>();
		List<MediaURL> realMediaURLs = new ArrayList<>();

		try {

			/*
			 * Check for special site hooks
			 * Converts the initial URL in 1-n further urls
			 */
			logger.debug("1. Prefill phase-------------------------------------");
			SerializableMetadata preMeta = new SerializableMetadata();
			for (DataCollector col : preCollector) {
				if (col.matches(url)) {
					logger.debug("  call prefill on "+col.getClass().getSimpleName());
					realMediaURLs.addAll( col.preFillMetadata(url, preMeta) );
					logger.debug("  after "+col.getClass().getSimpleName()+" URLs are "+realMediaURLs);
				}
			}
			logger.debug("  after 1. meta is "+gson.toJson(preMeta));
			/*
			 * Ensure there is at least one entry in the list
			 */
			if (realMediaURLs.isEmpty()) {
				MediaURL mURL = new MediaURL(url, url.getPath());
				if (mURL.filename.lastIndexOf('/')>=0)
					mURL.filename = mURL.filename.substring(mURL.filename.lastIndexOf('/')+1);
				realMediaURLs.add(mURL);
			}
			logger.debug("  after 1. media URLs are: "+realMediaURLs);

			/*
			 * 2. Ensure that file is in library directory
			 */
			logger.debug("2. Copy URLs to library------------------------------");
			for (MediaURL mURL : realMediaURLs) {
				mURL.localFile = copyNonCollectionURLToLibrary(this, category, mURL);
			}

			/*
			 * 3. Load metadata from file
			 */
			logger.debug("3. Read metadata from files--------------------------");
			for (MediaURL mURL : realMediaURLs) {
				mURL.meta = MetadataUtil.readMetadata(mURL.filename, new FileInputStream(mURL.localFile.toFile()), mURL.localFile);
				/*
				 * At least obtain size
				 */
				if (mURL.meta.getWidth()==0) {
					logger.debug("   No size in metadata - read it from image itself");
					BufferedImage foo = ImageIO.read(mURL.localFile.toFile());
					if (foo!=null) {
						mURL.meta.setWidth(foo.getWidth());
						mURL.meta.setHeight(foo.getHeight());
						logger.debug("   image properties = "+Arrays.toString(foo.getPropertyNames()));
					}
				}
				mURL.meta.addFrom(preMeta);
				logger.debug("metadata from file = "+gson.toJson(mURL.meta));
			}


			/*
			 * 4. Create media
			 */
			logger.debug("4. Create media objects------------------------------");
			for (MediaURL mURL : realMediaURLs) {
				AbstractMedia media = null;
				switch (category) {
				case BATTLEMAP_STATIC:
					media = new BattleMapImpl(mURL.meta);
					break;
				case NPC:
					media = new ImageMediaImpl(mURL.meta);
					break;
				default:
					logger.error("Don't know what to do for "+category);
					return null;
				}
				media.setCategory(category);
				media.setRelativePath(localBaseDir.relativize(mURL.localFile));
				media.setUUID(UUID.randomUUID());
				media.setLibrary(this);
				media.setLicense(LicenseType.UNKNOWN);
				if (mURL.meta.getLicense()!=null)
					media.setLicense(mURL.meta.getLicense());
//				if (media.getSource()==null)
					media.setSource(url);
				if (media instanceof ImageMedia && mURL.meta.getImageType()!=null) {
					((ImageMedia)media).setType(mURL.meta.getImageType());
				}
				mURL.media = media;
				logger.debug("created media = "+gson.toJson(media));
			}

			/*
			 * 5. Assume some media data from URL
			 */
			logger.debug("5. Assume some media data----------------------------");
			for (MediaURL mURL : realMediaURLs) {
				if (mURL.media.getName()==null) {
					String filename = url.getPath();
					if (filename.lastIndexOf('/')>0)
						filename = filename.substring(filename.lastIndexOf('/')+1);
					// For collection URLs, remove suffix
					if (filename.lastIndexOf('.')>0)
						filename = filename.substring(0,filename.lastIndexOf('.'));
					// Replace _ with spaces
					StringBuffer buf = new StringBuffer(filename);
					for (int i=0; i<buf.length(); i++) {
						if (buf.charAt(i)=='_') {
							buf.replace(i, i+1, " ");
						}
					}
					mURL.media.setName(buf.toString());
				}
				if (mURL.media.getName().toLowerCase().contains("geomorph") && mURL.media instanceof TileDefinition)
					((TileDefinition)mURL.media).setGeomorph(true);
			}

			/*
			 * 6. Check for special site hooks
			 */
			logger.debug("6. Post fill-----------------------------------------");
			for (MediaURL mURL : realMediaURLs) {
				for (DataCollector col : postCollector) {
					if (col.matches(mURL.url))
						col.fillMetadata(mURL.url, mURL.media);
				}
			}

			/*
			 * 7. Add to SQL
			 */
			for (MediaURL mURL : realMediaURLs) {
				try {
					logger.info("7. Store metadata in SQL database");
					logger.debug(mURL.media);
					insertMedia(mURL.media);
				} catch (SQLException e) {
					logger.error("Failed adding media to database: "+e,e);
				}

				logger.info("Imported "+mURL.media);
				ret.add(mURL.media);

				// Fire events
				logger.debug("7b. Fire events-----------------------------------------");
				for (MediaLibraryListener callback : listener) {
					try {
						callback.mediaAdded(mURL.media);
					} catch (Exception e) {
						logger.error("Error delivering media event");
					}
				}
			}
		} finally {
			logger.debug("STOP : importURL("+category+", "+url+")");
		}

		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.MediaLibrary#getThumbnail(de.rpgframework.media.ImageMedia)
	 */
	@Override
	public byte[] getThumbnail(ImageMedia img) {
//		logger.warn("IMPROVE: getThumbnail");
		return MediaHelper.getThumbnail(img);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.WritableMediaLibrary#addListener(de.rpgframework.media.MediaLibraryListener)
	 */
	@Override
	public void addListener(MediaLibraryListener callback) {
		if (!listener.contains(callback))
			listener.add(callback);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.WritableMediaLibrary#removeListener(de.rpgframework.media.MediaLibraryListener)
	 */
	@Override
	public void removeListener(MediaLibraryListener callback) {
		listener.remove(callback);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.WritableMediaLibrary#delete(de.rpgframework.media.Media)
	 */
	@Override
	public String delete(Media media) {
		logger.info("Delete "+media);
		try {
			delete.setString(1, media.getUUID().toString());
			logger.debug("SQL: "+delete);
			delete.execute();

			Path file = localBaseDir.resolve(media.getRelativePath());
			Files.delete(file);
			return null;
		} catch (SQLException e) {
			return "Failed deleting from database: "+e;
		} catch (IOException e) {
			return "Failed deleting from filesyste,: "+e;
		}

	}

	//-------------------------------------------------------------------
	/**
	 * @throws IOException
	 * @see de.rpgframework.media.WritableMediaLibrary#merge(de.rpgframework.media.RoleplayingMetadata.Category, java.util.List)
	 */
	@Override
	public MediaCollection<? extends Media> merge(Category cat, List<? extends Media> medias) throws IOException {
		logger.debug("merge to new "+cat+": "+medias);
		// Check media types
		switch (cat) {
		case BATTLEMAPSET:
			if (medias.stream().anyMatch(ev -> ev.getCategory()!=Category.BATTLEMAP_STATIC))
				throw new IllegalArgumentException("Can only combine battlemaps to a battlemapset");
			break;
		case TILESET:
			if (medias.stream().anyMatch(ev -> ev.getCategory()!=Category.MAPTILE))
				throw new IllegalArgumentException("Can only combine maptiles to a tileset");
			break;
		default:
			throw new IllegalArgumentException("Must be BATTLEMAPSET or TILESET");
		}

		/*
		 * Create a directory for the set
		 */
		String title  = medias.get(0).getName();
		String artist = medias.get(0).getArtist();
		String series = medias.get(0).getSeries();
		if (title==null)
			throw new IOException("Title must be set");
		List<String> elements = new ArrayList<>();
		if (artist!=null) elements.add(toCamelCase(artist));
		if (series!=null) elements.add(toCamelCase(series));
		elements.add(toCamelCase(title));
		String directoryName = String.join("_", elements);

		Path newDirectory = localBaseDir.resolve("tileset").resolve(directoryName);
		if (Files.exists(newDirectory))
			throw new IOException("Collection with that name already exists in this library");

		logger.info("1. Create directory "+newDirectory.toAbsolutePath());
		newDirectory = Files.createDirectories(newDirectory);

		/*
		 * Create collection object
		 */
		Path infoFile = null;
		MediaCollection<? extends Media> set = null;
		switch (cat) {
		case BATTLEMAPSET:
			set = new BattlemapSetImpl(newDirectory);
			infoFile = newDirectory.resolve("battlemapset.json");
			// Add files
			for (Media m : medias) ((BattlemapSetImpl)set).add( (Battlemap) m);
			break;
		case TILESET:
			set = new TilesetImpl(newDirectory);
			infoFile = newDirectory.resolve("tileset.json");
			// Add files
			for (Media m : medias) ((TilesetImpl)set).add( (TileDefinition) m);
			break;
		default:
			logger.warn("Don't know how to create collection of type: "+cat);
			break;
		}

		/*
		 * Set common data
		 */

		((AbstractMedia)set).setLibrary(this);
		((AbstractMedia)set).setUUID(UUID.randomUUID());
		set.setArtist(artist);
		set.setSeries(series);
		set.setSource(medias.get(0).getSource());
		set.setName(title);
		set.setLicense(medias.get(0).getLicense());

		/*
		 * Create a info structure
		 */
		logger.info("2. Store tileset metadata to "+newDirectory.toAbsolutePath());
		// Store relative path in media
		try {
			set.setRelativePath(localBaseDir.relativize(infoFile));
			String json = gson.toJson(set);
			BufferedWriter out = Files.newBufferedWriter(infoFile, StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE);
			out.write(json);
			out.close();
			logger.debug("   Wrote "+infoFile);
		} catch (Throwable e) {
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			logger.error("Failed to write JSON file to "+infoFile+": "+e);
			logger.error("\n"+out.toString().substring(0, 1200));
			System.exit(0);
		}

		/*
		 * Add to SQL
		 */
		try {
			logger.info("3. Store tileset metadata in SQL");
			insertMedia(set);
		} catch (SQLException e) {
			logger.error("Failed adding media to database: "+e);
			System.exit(0);
		}

		logger.info("Created new "+cat+": "+set);
		return set;
	}

}
