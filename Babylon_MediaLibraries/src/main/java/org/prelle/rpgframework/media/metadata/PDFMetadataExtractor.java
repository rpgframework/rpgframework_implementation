/**
 * 
 */
package org.prelle.rpgframework.media.metadata;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import javax.imageio.ImageIO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.pdmodel.graphics.optionalcontent.PDOptionalContentProperties;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;

/**
 * @author prelle
 *
 */
public class PDFMetadataExtractor implements MetadataExtractor {

	private final static Logger logger = LogManager.getLogger("babylon.media.meta");
	
	private final static String[] SUFFIX = new String[] {"pdf"};
	
//	private Gson gson;

	//-------------------------------------------------------------------
	public PDFMetadataExtractor() {
//		gson = new Gson();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#getSupportedSuffixes()
	 */
	@Override
	public List<String> getSupportedSuffixes() {
		return Arrays.asList(SUFFIX);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#readMetadata(java.io.InputStream)
	 */
	@Override
	public SerializableMetadata readMetadata(Path localFile) {
		try {
			return readMetadata(new FileInputStream(localFile.toFile()));
		} catch (FileNotFoundException e) {
			logger.error("Failed reading metadata from file "+localFile,e);
			return new SerializableMetadata();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#readMetadata(java.io.InputStream)
	 */
	@Override
	public SerializableMetadata readMetadata(InputStream in) {
		SerializableMetadata media = new SerializableMetadata();

		try {
			PDDocument document = PDDocument.load(in);
			
			PDDocumentInformation info = document.getDocumentInformation();
			media.setTitle(info.getTitle());
			media.setArtist(info.getAuthor());
			if (info.getKeywords()!=null) {
				logger.debug("parse keywords: "+info.getKeywords());
				StringTokenizer tok = new StringTokenizer(info.getKeywords(), ",");
				List<String> keywords = new ArrayList<>();
				while (tok.hasMoreTokens())
					keywords.add(tok.nextToken());
			}
			
			/* Layer */
			List<String> layerNames = new ArrayList<>();
			PDOptionalContentProperties ocprops = document.getDocumentCatalog().getOCProperties();
			for (String groupName : ocprops.getGroupNames()) {
				layerNames.add(groupName);
			}
			media.setLayerNames(layerNames);
			
			/*
			 * Try read image
			 */
			int pageToRender = (document.getNumberOfPages()>1)?1:0;
			PDPage page = document.getPage(pageToRender);
			PDResources resources = page.getResources();
//			int count=0;
			for (COSName objName : resources.getXObjectNames()) {
				logger.debug("++++++"+objName+" = "+resources.getXObject(objName));
				PDXObject obj = resources.getXObject(objName);
				if (obj instanceof PDImageXObject) {
					PDImageXObject image = (PDImageXObject)obj;
					logger.debug("   "+image.getWidth()+"x"+image.getHeight()+" ");
					media.setWidth(image.getWidth());
					media.setHeight(image.getHeight());
					break;
					
//					File filename = new File("/tmp/foo-"+layerNames.get(count)+".png");
//					ImageIO.write(image.getImage(), "PNG", filename);
//					SerializableMetadata imgMeta = MetadataUtil.readMetadata(filename.getName(), new FileInputStream(filename));
//					logger.debug("meta = "+gson.toJson(imgMeta));
//					count++;
				}
//				PDXObjectImage image = (PDXObjectImage) resources.getXObject(objName);
			}
		} catch (IOException e) {
			logger.error("Error reading PDF data",e);
		}
		return media;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#writeMetadata(org.prelle.rpgframework.media.metadata.SerializableMetadata, java.io.InputStream, java.io.OutputStream)
	 */
	@Override
	public void writeMetadata(SerializableMetadata media, Path imageFile) throws IOException {
		logger.debug("START writeMetadata");
		try {
			logger.warn("writing metadata to PDF not supported");
		} finally {
			logger.debug("STOP  writeMetadata to "+imageFile);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#readThumbnail(java.io.InputStream)
	 */
	@Override
	public byte[] readThumbnail(Path localFile) {
		logger.debug("START readThumbnail");
		try {
			PDDocument document = PDDocument.load(localFile.toFile());
			PDFRenderer pdfRenderer = new PDFRenderer(document);
			
			int pageToRender = (document.getNumberOfPages()>1)?1:0;
			BufferedImage original = pdfRenderer.renderImageWithDPI(pageToRender, 50, ImageType.RGB);
			
			double scaleX = 150.0 / original.getWidth();
			double scaleY = 150.0 / original.getHeight();
			double factor = Math.min(scaleX, scaleY);
			int newWidth = Double.valueOf(original.getWidth() * factor).intValue();
			int newHeight = Double.valueOf(original.getHeight() * factor).intValue();
			
			BufferedImage resized = new BufferedImage(newWidth, newHeight, original.getType());
			Graphics2D g = resized.createGraphics();
			g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
			    RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			g.drawImage(original, 0, 0, newWidth, newHeight, 0, 0, original.getWidth(),
			    original.getHeight(), null);
			g.dispose();
			
			document.close();
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write( resized, "jpg", baos );
			baos.flush();
			byte[] imageInByte = baos.toByteArray();
			baos.close();
			return imageInByte;
		} catch (IOException e) {
			logger.error("Error generating thumbnail");
			return null;
		} finally {
			logger.debug("STOP  readThumbnail");
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#writeThumbnail(byte[], java.nio.file.Path)
	 */
	@Override
	public void writeThumbnail(byte[] data, Path imageFile) throws IOException {
		logger.debug("START writeThumbnail");
		String suffix = imageFile.getFileName().toString();
		suffix = suffix.substring(suffix.lastIndexOf(".")+1, suffix.length());
		Path temporarySrc = Files.createTempFile("babylon", suffix);
		// Copy file
		Files.copy(new FileInputStream(imageFile.toFile()), temporarySrc, StandardCopyOption.REPLACE_EXISTING);
		
	}
	
}
