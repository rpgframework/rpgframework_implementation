package org.prelle.rpgframework.media.collector;

class ImgurImage {
	private String id;
    private String title;
    private String caption;
    private String hash;
    private String datetime;
    private String type;
    private boolean animated;
    private int width;
    private int height;
    private long size;
    private long views;
    private long bandwidth;
    private String deletehash;
    private String link;

    public String getTitle() {
        return title;
    }

    public String getCaption() {
        return caption;
    }

    public String getHash() {
        return hash;
    }

    public String getDatetime() {
        return datetime;
    }

    public String getType() {
        return type;
    }

    public boolean isAnimated() {
        return animated;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public long getSize() {
        return size;
    }

    public long getViews() {
        return views;
    }

    public long getBandwidth() {
        return bandwidth;
    }

    public String getDeletehash() {
        return deletehash;
    }

	//-------------------------------------------------------------------
	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
}