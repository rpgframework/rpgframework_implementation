/**
 * 
 */
package org.prelle.rpgframework.media.metadata;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * @author prelle
 *
 */
public class XMPParser {

	private final static Logger logger = LogManager.getLogger("babylon.media.meta");

	private static DocumentBuilder builder;
	
	//-------------------------------------------------------------------
	static {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//-------------------------------------------------------------------
	private static String findTextContent(Node node) {
		switch (node.getNodeType()) {
		case Node.TEXT_NODE:
			String txt2 = node.getTextContent();
			if (txt2!=null && !txt2.trim().isEmpty())
				return txt2;
			return null;
		case Node.ELEMENT_NODE:
			NodeList list = ((Element)node).getChildNodes();
			for (int i=0; i<list.getLength(); i++) {
				String txt = findTextContent(list.item(i));
				if (txt!=null)
					return txt;
			}
			return null;
		}
		
		return null;
	}

	//-------------------------------------------------------------------
	public static void parseInto(SerializableMetadata meta, String xmp) throws SAXException, IOException {
		Document doc = builder.parse(new InputSource(new StringReader(xmp)));
		
		// Title
		NodeList list = doc.getElementsByTagNameNS("http://purl.org/dc/elements/1.1/", "title");
		if (list!=null && list.getLength()>0) {
			String title = findTextContent((Element) list.item(0));
			if (title!=null && !title.isEmpty()) {
				logger.debug("XMP title = "+title+"    current value="+meta.getTitle());
				if (meta.getTitle()==null)
					meta.setTitle(title);
			}
		}
		
		// Title
		list = doc.getElementsByTagNameNS("http://purl.org/dc/elements/1.1/", "creator");
		if (list!=null && list.getLength()>0) {
			String value = findTextContent((Element) list.item(0));
			if (value!=null && !value.isEmpty()) {
				logger.debug("XMP creator = "+value+"    current value="+meta.getArtist());
				if (meta.getArtist()==null)
					meta.setArtist(value);
			}
		}

	}

}
