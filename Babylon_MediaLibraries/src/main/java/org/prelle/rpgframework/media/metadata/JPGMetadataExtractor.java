/**
 * 
 */
package org.prelle.rpgframework.media.metadata;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.ImageWriteException;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.GenericImageMetadata.GenericImageMetadataItem;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.common.ImageMetadata.ImageMetadataItem;
import org.apache.commons.imaging.common.RationalNumber;
import org.apache.commons.imaging.common.bytesource.ByteSource;
import org.apache.commons.imaging.common.bytesource.ByteSourceFile;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.jpeg.JpegPhotoshopMetadata;
import org.apache.commons.imaging.formats.jpeg.exif.ExifRewriter;
import org.apache.commons.imaging.formats.jpeg.iptc.IptcBlock;
import org.apache.commons.imaging.formats.jpeg.iptc.IptcRecord;
import org.apache.commons.imaging.formats.jpeg.iptc.IptcTypes;
import org.apache.commons.imaging.formats.jpeg.iptc.JpegIptcRewriter;
import org.apache.commons.imaging.formats.jpeg.iptc.PhotoshopApp13Data;
import org.apache.commons.imaging.formats.tiff.TiffField;
import org.apache.commons.imaging.formats.tiff.TiffImageMetadata;
import org.apache.commons.imaging.formats.tiff.constants.ExifTagConstants;
import org.apache.commons.imaging.formats.tiff.constants.TiffTagConstants;
import org.apache.commons.imaging.formats.tiff.taginfos.TagInfo;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputDirectory;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputSet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import de.rpgframework.media.ImageType;

/**
 * @author prelle
 *
 */
public class JPGMetadataExtractor implements MetadataExtractor {

	private final static Logger logger = LogManager.getLogger("babylon.media.meta");
	
	private final static String[] SUFFIX = new String[] {"jpg","jpeg","tif"};
	
	private Gson gson;

	//-------------------------------------------------------------------
	public JPGMetadataExtractor() {
		gson = new Gson();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#getSupportedSuffixes()
	 */
	@Override
	public List<String> getSupportedSuffixes() {
		return Arrays.asList(SUFFIX);
	}

	//-------------------------------------------------------------------
	private static String getTagValue(final JpegImageMetadata jpegMetadata, final TagInfo tagInfo) {
		final TiffField field = jpegMetadata.findEXIFValueWithExactMatch(tagInfo);
		if (field!=null) {
			String val = field.getValueDescription();
			if (val.startsWith("'"))
				val = val.substring(1);
			if (val.endsWith("'")) {
				val = val.substring(0, val.length()-1);
			}
			return val;
		}
		return null;
	}

	//-------------------------------------------------------------------
	private static boolean hasTagValue(final JpegImageMetadata jpegMetadata, final TagInfo tagInfo) {
		return jpegMetadata.findEXIFValueWithExactMatch(tagInfo)!=null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#readMetadata(java.io.InputStream)
	 */
	@Override
	public SerializableMetadata readMetadata(Path localFile) {
		try {
			SerializableMetadata media = parseImageMetadata(Imaging.getMetadata(localFile.toFile()));
			/*
			 * If size is unknown, open file
			 */
			if (media.getWidth()==0) {
				logger.debug("  get image size from file "+localFile);
				BufferedImage img = ImageIO.read(localFile.toFile());
				if (img!=null) {
					media.setWidth(img.getWidth());
					media.setHeight(img.getHeight());
				}
			}
			return media;
		} catch (Exception e) {
			logger.error("Failed reading metadata from file "+localFile,e);
			return new SerializableMetadata();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#readMetadata(java.io.InputStream)
	 */
	@Override
	public SerializableMetadata readMetadata(InputStream in) {
		try {
			return parseImageMetadata(Imaging.getMetadata(in, null));
		} catch (Exception e) {
			logger.error("Failed reading metadata from file stream",e);
			return new SerializableMetadata();
		}
	}

	//-------------------------------------------------------------------
	private SerializableMetadata parseImageMetadata(ImageMetadata metadata) {
		SerializableMetadata media = new SerializableMetadata();

		try {
			media.setImageType(ImageType.STATIC);
			if (metadata!=null) {
//				List<? extends ImageMetadataItem> items = metadata.getItems();
//				for (int i = 0; i < items.size(); i++) {
//					final ImageMetadataItem item = items.get(i);
//					logger.debug("    " + "item: " + item);
//				}

				if (metadata instanceof JpegImageMetadata) {
					JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;
					//					for (ImageMetadataItem dir : jpegMetadata.getExif().getDirectories()) {
					//						Directory direct = (Directory)dir;
					//						logger.debug("   X = "+direct);
					//					}

					// Artist
					if (hasTagValue(jpegMetadata, TiffTagConstants.TIFF_TAG_ARTIST)) {
						media.setArtist(getTagValue(jpegMetadata, TiffTagConstants.TIFF_TAG_ARTIST));
						logger.debug("  Set artist to: "+media.getArtist());
					}
					// Copyright
					if (hasTagValue(jpegMetadata, TiffTagConstants.TIFF_TAG_COPYRIGHT)) {
						media.setCopyright(getTagValue(jpegMetadata, TiffTagConstants.TIFF_TAG_COPYRIGHT));
						logger.debug("  Set copyright to: "+media.getCopyright());
					}
					// ImageDescription / Media title
					if (hasTagValue(jpegMetadata, TiffTagConstants.TIFF_TAG_IMAGE_DESCRIPTION)) {
						media.setTitle(getTagValue(jpegMetadata, TiffTagConstants.TIFF_TAG_IMAGE_DESCRIPTION));
						logger.debug("  Set name to: "+media.getTitle());
					}
					// DocumentName / Source URL
					if (hasTagValue(jpegMetadata, TiffTagConstants.TIFF_TAG_DOCUMENT_NAME)) {
						try {
							media.setSourceURL(new URL(getTagValue(jpegMetadata, TiffTagConstants.TIFF_TAG_DOCUMENT_NAME)));
							logger.debug("  Set origin to: "+media.getSourceURL());
						} catch (Exception e) {
							logger.warn("TIFF/EXIF document name is not an URL: "+getTagValue(jpegMetadata, TiffTagConstants.TIFF_TAG_DOCUMENT_NAME));
						}
					}
					// Unique media ID
					String idString =  getTagValue(jpegMetadata, ExifTagConstants.EXIF_TAG_IMAGE_UNIQUE_ID);
					if (idString!=null) {
						try {
							StringBuffer buf = new StringBuffer(idString);
							buf.insert(20, '-');
							buf.insert(16, '-');
							buf.insert(12, '-');
							buf.insert( 8, '-');
							media.setUUID(UUID.fromString(buf.toString()));
							logger.debug("  Set UUID to: "+media.getUUID());
						} catch (Exception e) {
							logger.warn("Image ID is not a UUID: "+getTagValue(jpegMetadata, ExifTagConstants.EXIF_TAG_IMAGE_UNIQUE_ID));
							media.setUUID(UUID.randomUUID());
						}
					}
					// DPI
					if (hasTagValue(jpegMetadata, TiffTagConstants.TIFF_TAG_XRESOLUTION)) {
						media.setDPI( (int)Float.parseFloat(getTagValue(jpegMetadata, TiffTagConstants.TIFF_TAG_XRESOLUTION)));
						logger.debug("  Set DPI to: "+media.getDPI());
					}
					// UserComment / Keywords
					String comment =  getTagValue(jpegMetadata, ExifTagConstants.EXIF_TAG_USER_COMMENT);
					if (comment!=null && comment.startsWith("KEYWORDS: ")) {
						List<String> keywords = new ArrayList<>();
						for (String val : comment.substring(10).split(","))
							keywords.add(val);
						media.setKeywords(keywords);
						logger.debug("  Set keywords to: "+media.getKeywords());
					}
					// MakerNote / JSON for categories, type, genre ...
					String json =  getTagValue(jpegMetadata, ExifTagConstants.EXIF_TAG_MAKER_NOTE);
					if (json!=null) {
						logger.warn("TODO: deal with JSON: json");
						try {
							return gson.fromJson(json, SerializableMetadata.class);
						} catch (JsonSyntaxException e) {
							logger.debug("MAKER_NOTE in EXIF wasn't expected JSON");
						}
					}
					// Light source // Daylight, Night, Foggy, Snow, Rain
					if (hasTagValue(jpegMetadata, ExifTagConstants.EXIF_TAG_LIGHT_SOURCE))
						logger.warn("TODO: deal with Light source: "+getTagValue(jpegMetadata, ExifTagConstants.EXIF_TAG_LIGHT_SOURCE));

					//					printTagValue(jpegMetadata, TiffTagConstants.TIFF_TAG_XRESOLUTION);
					//					printTagValue(jpegMetadata, TiffTagConstants.TIFF_TAG_DATE_TIME);
					//					printTagValue(jpegMetadata, TiffTagConstants.TIFF_TAG_DOCUMENT_NAME);
					//					printTagValue(jpegMetadata, TiffTagConstants.TIFF_TAG_IMAGE_DESCRIPTION);
					//					printTagValue(jpegMetadata, TiffTagConstants.TIFF_TAG_ARTIST);
					//					printTagValue(jpegMetadata, TiffTagConstants.TIFF_TAG_COPYRIGHT);
					//					printTagValue(jpegMetadata, TiffTagConstants.TIFF_TAG_XMP);
					//
					//					printTagValue(jpegMetadata, ExifTagConstants.EXIF_TAG_MAKER_NOTE);
					//					printTagValue(jpegMetadata, ExifTagConstants.EXIF_TAG_USER_COMMENT);
					//					printTagValue(jpegMetadata, ExifTagConstants.EXIF_TAG_IMAGE_UNIQUE_ID);
					//					printTagValue(jpegMetadata, ExifTagConstants.EXIF_TAG_LIGHT_SOURCE);
					JpegPhotoshopMetadata itpc = jpegMetadata.getPhotoshop();
					if (itpc!=null) {
						List<String> keywords = new ArrayList<>();
						for (ImageMetadataItem field : itpc.getItems()) {
							logger.debug("    ...IPTC "+field);
							GenericImageMetadataItem item = (GenericImageMetadataItem) field;
							// Constants see IptcTypes
							switch (item.getKeyword()) {
							case "Keywords": keywords.add(new String(item.getText().getBytes("iso-8859-1"), "UTF-8")); break;
							case "Object Name": media.setTitle(new String(item.getText().getBytes("iso-8859-1"), "UTF-8")); break;
							case "Copyright Notice": media.setCopyright(new String(item.getText().getBytes("iso-8859-1"), "UTF-8")); break;
							}
						}
						media.setKeywords(keywords);

					}
					logger.debug("    XMP = "+getTagValue(jpegMetadata, TiffTagConstants.TIFF_TAG_XMP));
//					if (jpegMetadata.getEXIFThumbnailData().length>0) {
//						logger.debug("  found thumbnail: "+jpegMetadata.getEXIFThumbnailSize());
//						media.setThumbnail(jpegMetadata.getEXIFThumbnailData());
//					}
					
				}
			} else {
				logger.debug("  NO METADATA");
			}
			
		} catch (IOException e) {
			logger.error("Error reading image data",e);
		}
		return media;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#writeMetadata(org.prelle.rpgframework.media.metadata.SerializableMetadata, java.io.InputStream, java.io.OutputStream)
	 */
	@Override
	public void writeMetadata(SerializableMetadata media, Path imageFile) throws IOException {
		logger.debug("START writeMetadata");
		String suffix = imageFile.getFileName().toString();
		suffix = suffix.substring(suffix.lastIndexOf(".")+1, suffix.length());
		Path temporarySrc = Files.createTempFile("babylon", suffix);
		// Copy file
		Files.copy(new FileInputStream(imageFile.toFile()), temporarySrc, StandardCopyOption.REPLACE_EXISTING);
		
		try {
			/*
			 * Read eventually existing previous data
			 */
			TiffOutputSet outputSet = null;
//			JpegPhotoshopMetadata iptc = null;
			final ImageMetadata metadata = Imaging.getMetadata(imageFile.toFile());
			final JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;
			if (null != jpegMetadata) {
				final TiffImageMetadata exif = jpegMetadata.getExif();

				if (null != exif) {
					outputSet = exif.getOutputSet();
				}
//				iptc = jpegMetadata.getPhotoshop();
			}
			// If no previous tag set found, create one
			if (null == outputSet) {
				outputSet = new TiffOutputSet();
			}

			/*
			 * Modify EXIF tags
			 * Remove headers to overwrite and add them anew
			 */
			final TiffOutputDirectory exifDirectory = outputSet.getOrCreateExifDirectory();
			final TiffOutputDirectory tiffDirectory = outputSet.getOrCreateRootDirectory();
			// Write media UUID
			exifDirectory.removeField(ExifTagConstants.EXIF_TAG_IMAGE_UNIQUE_ID);
			if (media.getUUID()!=null)
				exifDirectory.add(ExifTagConstants.EXIF_TAG_IMAGE_UNIQUE_ID, media.getUUID().toString().replaceAll("-", ""));
			// Write variant
			exifDirectory.removeField(ExifTagConstants.EXIF_TAG_LIGHT_SOURCE);
			exifDirectory.add(ExifTagConstants.EXIF_TAG_LIGHT_SOURCE, (short)0);
			// Write keywords
			exifDirectory.removeField(ExifTagConstants.EXIF_TAG_USER_COMMENT);
			exifDirectory.add(ExifTagConstants.EXIF_TAG_USER_COMMENT, String.join(", ", media.getKeywords()));
			// Write artist
			tiffDirectory.removeField(TiffTagConstants.TIFF_TAG_ARTIST);
			if (media.getArtist()!=null)
				tiffDirectory.add(TiffTagConstants.TIFF_TAG_ARTIST, media.getArtist());
			// Write image description // Name
			tiffDirectory.removeField(TiffTagConstants.TIFF_TAG_IMAGE_DESCRIPTION);
			if (media.getTitle()!=null)
				tiffDirectory.add(TiffTagConstants.TIFF_TAG_IMAGE_DESCRIPTION, media.getTitle());
			// Source URL
			tiffDirectory.removeField(TiffTagConstants.TIFF_TAG_DOCUMENT_NAME);
			if (media.getSourceURL()!=null)
				tiffDirectory.add(TiffTagConstants.TIFF_TAG_DOCUMENT_NAME, media.getSourceURL().toExternalForm());
			// Write DPI
			tiffDirectory.removeField(TiffTagConstants.TIFF_TAG_XRESOLUTION);
			tiffDirectory.add(TiffTagConstants.TIFF_TAG_XRESOLUTION, RationalNumber.valueOf(media.getDPI()));
			// Write MediaTags / MakerNote
			exifDirectory.removeField(ExifTagConstants.EXIF_TAG_MAKER_NOTE);
			logger.debug("write MAKER_NOTE : "+gson.toJson(media));
			exifDirectory.add(ExifTagConstants.EXIF_TAG_MAKER_NOTE, gson.toJson(media).getBytes());

			/*
			 * Update file
			 */
			try (
					FileOutputStream fos = new FileOutputStream(imageFile.toFile());
					OutputStream os = new BufferedOutputStream(fos);
					) {
				new ExifRewriter().updateExifMetadataLossless(temporarySrc.toFile(), os, outputSet);
				logger.debug("Wrote "+imageFile.toAbsolutePath());
			}

			/*
			 * Modify ITPC Header
			 */
			logger.debug("Write IPTC header");
			writeIPTCData(imageFile, media);
			
		} catch (ImageReadException | ImageWriteException e) {
			logger.error("Failed reading/writing EXIF header",e);
			throw new IOException(e);
		} finally {
			logger.debug("STOP  writeEXIF to "+imageFile);
			Files.deleteIfExists(temporarySrc);
		}
	}

	//-------------------------------------------------------------------
	private static void writeIPTCData(Path dstFile, SerializableMetadata media) throws IOException {
		logger.debug("START writeIPTCData to "+dstFile);
		Path temporarySrc = Files.createTempFile(dstFile.getFileName().toString(), "tmp");
		try {
			Files.copy(new FileInputStream(dstFile.toFile()), temporarySrc, StandardCopyOption.REPLACE_EXISTING);
			File srcFile = temporarySrc.toFile();

			if (srcFile==null) return;

			ImageMetadata abstractMeta = Imaging.getMetadata(srcFile);
			if (abstractMeta==null || !(abstractMeta instanceof JpegImageMetadata))
				return;
			JpegImageMetadata jpegMetadata = (JpegImageMetadata)abstractMeta;

			/*
			 * Modify ITPC Header
			 */
			List<IptcBlock> newBlocks = new ArrayList<>();
			List<IptcRecord> newRecords = new ArrayList<>();			
			JpegPhotoshopMetadata iptc = jpegMetadata.getPhotoshop();
			if (iptc!=null) {
				newBlocks.addAll(iptc.photoshopApp13Data.getNonIptcBlocks());	
				for (IptcRecord record : iptc.photoshopApp13Data.getRecords()) {
					if (record.iptcType != IptcTypes.OBJECT_NAME
							&& record.iptcType != IptcTypes.CREDIT
							&& record.iptcType != IptcTypes.KEYWORDS
							&& record.iptcType != IptcTypes.COPYRIGHT_NOTICE) {
						newRecords.add(record);
					}
				}
			}

			// Write new data
			if (media.getTitle()!=null)
				newRecords.add(new IptcRecord(IptcTypes.OBJECT_NAME, media.getTitle()));
			if (media.getArtist()!=null)
				newRecords.add(new IptcRecord(IptcTypes.CREDIT, media.getArtist()));
			if (media.getCopyright()!=null)
				newRecords.add(new IptcRecord(IptcTypes.COPYRIGHT_NOTICE, media.getCopyright()));
			for (String keyword : media.getKeywords()) 
				newRecords.add(new IptcRecord(IptcTypes.KEYWORDS, keyword));

			/*
			 * Write to file
			 */
			PhotoshopApp13Data newData = new PhotoshopApp13Data(newRecords, newBlocks);
			ByteSource byteSource = new ByteSourceFile(srcFile);
			final File updated = dstFile.toFile();
			try (FileOutputStream fos = new FileOutputStream(updated);
					OutputStream os = new BufferedOutputStream(fos)) {
				new JpegIptcRewriter().writeIPTC(byteSource, os, newData);
			}
		} catch (Exception e) {
			logger.error("FAIL  writeIPTCData",e);
		} finally {
			logger.debug("STOP  writeIPTCData");
			Files.deleteIfExists(temporarySrc);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#readThumbnail(java.io.InputStream)
	 */
	@Override
	public byte[] readThumbnail(Path localFile) {
//		logger.debug("readThumbnail");
		try {
			ImageMetadata metadata = Imaging.getMetadata(localFile.toFile());
			if (metadata!=null) {
				if (metadata instanceof JpegImageMetadata) {
					JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;
					if (jpegMetadata.getEXIFThumbnailData()!=null && jpegMetadata.getEXIFThumbnailData().length>0) {
						return jpegMetadata.getEXIFThumbnailData();
					}
				}
			} 
			
			return MetadataUtil.createThumbnailFromImage(new FileInputStream(localFile.toFile()));

		} catch (IOException | ImageReadException e) {
			logger.warn("Failed obtaining thumbnail: "+e);
		} finally {
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#writeThumbnail(byte[], java.nio.file.Path)
	 */
	@Override
	public void writeThumbnail(byte[] data, Path imageFile) throws IOException {
		logger.debug("START writeThumbnail");
		String suffix = imageFile.getFileName().toString();
		suffix = suffix.substring(suffix.lastIndexOf(".")+1, suffix.length());
		Path temporarySrc = Files.createTempFile("babylon", suffix);
		// Copy file
		Files.copy(new FileInputStream(imageFile.toFile()), temporarySrc, StandardCopyOption.REPLACE_EXISTING);
		
	}
	
}
