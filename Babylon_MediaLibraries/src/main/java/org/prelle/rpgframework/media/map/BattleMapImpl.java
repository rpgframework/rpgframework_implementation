/**
 * 
 */
package org.prelle.rpgframework.media.map;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.prelle.rpgframework.media.ImageMediaImpl;
import org.prelle.rpgframework.media.metadata.SerializableMetadata;

import de.rpgframework.media.map.Battlemap;

/**
 * @author prelle
 *
 */
public class BattleMapImpl extends ImageMediaImpl implements Battlemap {
	
	private int offsetX;
	private int offsetY;
	private List<String> layerNames;

	//-------------------------------------------------------------------
	public BattleMapImpl(Path file, Category cat) {
		super(file, cat);
		layerNames = new ArrayList<String>();
	}
	
	//-------------------------------------------------------------------
	public BattleMapImpl(SerializableMetadata meta) {
		super(meta);
		layerNames = meta.getLayerNames();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.map.Battlemap#getLayer()
	 */
	@Override
	public List<String> getLayer() {
		return layerNames;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the offsetX
	 */
	public int getOffsetX() {
		return offsetX;
	}

	//-------------------------------------------------------------------
	/**
	 * @param offsetX the offsetX to set
	 */
	public void setOffsetX(int offsetX) {
		this.offsetX = offsetX;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the offsetY
	 */
	public int getOffsetY() {
		return offsetY;
	}

	//-------------------------------------------------------------------
	/**
	 * @param offsetY the offsetY to set
	 */
	public void setOffsetY(int offsetY) {
		this.offsetY = offsetY;
	}

}
