/**
 * 
 */
package org.prelle.rpgframework.media.metadata;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;

import de.rpgframework.media.ImageType;

/**
 * @author prelle
 *
 */
public class CatchAllMetadataExtractor implements MetadataExtractor {

	protected Logger logger = LogManager.getLogger("babylon.media.metadata");
	
	private final static String[] SUFFIX = new String[] {
			"jpg","jpeg","png","gif","tif","psd",
			"m4v","avi","mov",
			"wav","mp3"
			};

	//-------------------------------------------------------------------
	public CatchAllMetadataExtractor() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#getSupportedSuffixes()
	 */
	@Override
	public List<String> getSupportedSuffixes() {
		return Arrays.asList(SUFFIX);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#readMetadata(java.io.InputStream)
	 */
	@Override
	public SerializableMetadata readMetadata(Path localFile) {
		try {
			return parseMetadata( ImageMetadataReader.readMetadata(localFile.toFile()));
		} catch (Exception e) {
			logger.error("Failed parsing metadata from file "+localFile,e);
		}
		return new SerializableMetadata();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#readMetadata(java.io.InputStream)
	 */
	@Override
	public SerializableMetadata readMetadata(InputStream in) {
		try {
			return parseMetadata( ImageMetadataReader.readMetadata(in));
		} catch (Exception e) {
			logger.error("Failed parsing metadata from stream",e);
		}
		return new SerializableMetadata();
	}

	//-------------------------------------------------------------------
	private SerializableMetadata parseMetadata(Metadata metadata) {
		SerializableMetadata media = new SerializableMetadata();

		try {
			for (Directory directory : metadata.getDirectories()) {
			    for (Tag tag : directory.getTags()) {
			        logger.debug(String.format("[%s] - %s = %s",
			            directory.getName(), tag.getTagName(), tag.getDescription()));
			        if ("JPEG".equals(directory.getName())) {
			        	StringTokenizer tok = new StringTokenizer(tag.getDescription());
			        	switch (tag.getTagName()) {
			        	case "Image Width": media.setWidth(Integer.parseInt(tok.nextToken())); break;
			        	case "Image Height": media.setHeight(Integer.parseInt(tok.nextToken())); break;
			        	}
			        } else if ("JFIF".equals(directory.getName())) {
			        	StringTokenizer tok = new StringTokenizer(tag.getDescription());
			        	switch (tag.getTagName()) {
			        	case "X Resolution": media.setDPI(Integer.parseInt(tok.nextToken())); break;
			        	}
			        } else if ("Photoshop".equals(directory.getName())) {
//			        	StringTokenizer tok = new StringTokenizer(tag.getDescription());
			        	switch (tag.getTagName()) {
			        	case "Slices":
			        		if (media.getTitle()==null || media.getTitle().isEmpty())
			        			media.setTitle(tag.getDescription()); 
			        		break;			        		
			        	}
			        } else if ("MP4 Video".equals(directory.getName())) {
			        	StringTokenizer tok = new StringTokenizer(tag.getDescription());
			        	switch (tag.getTagName()) {
			        	case "Width": media.setWidth(Integer.parseInt(tok.nextToken())); break;
			        	case "Height": media.setHeight(Integer.parseInt(tok.nextToken())); break;
			        	case "Horizontal Resolution": media.setDPI(Integer.parseInt(tok.nextToken())); break;
			        	}
			        } else if ("File Type".equals(directory.getName())) {
//			        	StringTokenizer tok = new StringTokenizer(tag.getDescription());
			        	switch (tag.getTagName()) {
			        	case "Detected MIME Type":
			        		if (tag.getDescription().startsWith("video"))
			        			media.setImageType(ImageType.VIDEO);
			        		else if (tag.getDescription().startsWith("image"))
			        			media.setImageType(ImageType.STATIC);
			        		break;
			        	}
			        }
			    }
			    if (directory.hasErrors()) {
			        for (String error : directory.getErrors()) {
			            System.err.format("ERROR: %s", error);
			        }
			    }
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return media;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#writeMetadata(org.prelle.rpgframework.media.metadata.SerializableMetadata, java.io.InputStream, java.io.OutputStream)
	 */
	@Override
	public void writeMetadata(SerializableMetadata media, Path imageFile) throws IOException {
		throw new IOException("Writing metadata not supported");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#readThumbnail(java.io.InputStream)
	 */
	@Override
	public byte[] readThumbnail(Path localFile) {
		logger.warn("TODO: readThumbnail");
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#writeThumbnail(byte[], java.nio.file.Path)
	 */
	@Override
	public void writeThumbnail(byte[] data, Path imageFile) throws IOException {
		throw new IOException("Writing thumbnails not supported");
	}
	
}
