package org.prelle.rpgframework.media.metadata;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.Base64;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.metadata.IIOMetadataFormatImpl;
import javax.imageio.metadata.IIOMetadataNode;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jcodec.api.FrameGrab;
import org.jcodec.common.model.Picture;
import org.jcodec.containers.mp4.boxes.MetaValue;
import org.jcodec.javase.scale.AWTUtil;
import org.jcodec.movtool.MetadataEditor;
import org.w3c.dom.NodeList;

import de.rpgframework.media.ImageType;

/**
 * @author prelle
 *
 */
public class JCodecVideoMetadataExtractor implements MetadataExtractor {

	private final static String UUID     = "rpgframework:uuid";
	private final static String CATEGORY = "rpgframework:category";
	private final static String ARTIST   = "rpgframework:artist";
	private final static String SERIES   = "rpgframework:series";
	private final static String TITLE    = "rpgframework:title";
	private final static String SOURCE_URL = "rpgframework:sourceURL";
	private final static String COPYRIGHT  = "rpgframework:copyright";
	private final static String LICENSE    = "rpgframework:license";
	private final static String KEYWORDS   = "rpgframework:keywords";
//	private final static String TAGS       = "rpgframework:tags";
	private final static String THUMBNAIL  = "rpgframework:thumbnail";

	private final static Logger logger = LogManager.getLogger("babylon.media.meta");

	private final static String[] SUFFIX = new String[] {"m4v","avi","mkv"};

//	private Gson gson;

	//-------------------------------------------------------------------
	public JCodecVideoMetadataExtractor() {
//		gson = new Gson();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#getSupportedSuffixes()
	 */
	@Override
	public List<String> getSupportedSuffixes() {
		return Arrays.asList(SUFFIX);
	}

	//-------------------------------------------------------------------
	private static String fourccToString(int key) {
		try {
			byte[] bytes = new byte[4];
			ByteBuffer.wrap(bytes).order(ByteOrder.BIG_ENDIAN).putInt(key);
			return new String(bytes, "iso8859-1");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @throws MalformedURLException
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#readMetadata(java.io.InputStream)
	 */
	@Override
	public SerializableMetadata readMetadata(InputStream in) {
		SerializableMetadata meta = new SerializableMetadata();
		return meta;
	}

	//-------------------------------------------------------------------
	/**
	 * @throws MalformedURLException
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#readMetadata(java.io.InputStream)
	 */
	@Override
	public SerializableMetadata readMetadata(Path remoteFile) {
		SerializableMetadata meta = new SerializableMetadata();

		try {
			MetadataEditor mediaMeta = MetadataEditor.createFrom(remoteFile.toFile());
			meta.setImageType(ImageType.VIDEO);
			Map<String, MetaValue> keyedMeta = mediaMeta.getKeyedMeta();
			if (keyedMeta != null) {
				logger.debug("Keyed metadata:");
				for (Entry<String, MetaValue> entry : keyedMeta.entrySet()) {
					logger.debug(entry.getKey() + ": " + entry.getValue());
				}
			}

			Map<Integer, MetaValue> itunesMeta = mediaMeta.getItunesMeta();
			if (itunesMeta != null) {
				logger.debug("iTunes metadata:");
				for (Entry<Integer, MetaValue> entry : itunesMeta.entrySet()) {
					String key = fourccToString(entry.getKey());
					logger.debug(key + " = \t" + entry.getValue()+"   \t"+entry.getValue().getClass());
					switch (key) {
					case "desc": meta.setTitle(entry.getValue().getString()); break;
					case "aART": meta.setArtist(entry.getValue().getString()); break;
					case "covr":
						logger.debug("   Found cover image of "+entry.getValue().getData().length+" bytes");
						SerializableMetadata coverMeta = (new JPGMetadataExtractor()).readMetadata(new ByteArrayInputStream(entry.getValue().getData()));
						if (coverMeta!=null && meta.getDPI()==0)
							meta.setDPI(coverMeta.getDPI());
						break;
					}
				}
			}
		} catch (IOException e) {
			logger.error("Failed reading video metadata",e);
		}


		/**
		 * Read size from video
		 */
		if (meta.getWidth()==0) {
			try {
				Picture picture = FrameGrab.getFrameFromFile(remoteFile.toFile(), 100);
				//for JDK (jcodec-javase)
				BufferedImage bufferedImage = AWTUtil.toBufferedImage(picture);
				meta.setWidth(bufferedImage.getWidth());
				meta.setHeight(bufferedImage.getHeight());
			} catch (Exception e) {
				logger.error("Failed reading video size",e);
			}
		}
		return meta;
	}

	//	//-------------------------------------------------------------------
	//	private static String prettyPrint(Node node) {
	//		try {
	//			TransformerFactory tf = TransformerFactory.newInstance();
	//			Transformer transformer = tf.newTransformer();
	//			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	//
	//			StringWriter writer = new StringWriter();
	//			transformer.transform(new DOMSource(node), new StreamResult(writer));
	//			return writer.toString();
	//		} catch (IllegalArgumentException | TransformerFactoryConfigurationError | TransformerException e) {
	//			// TODO Auto-generated catch block
	//			e.printStackTrace();
	//		}
	//		return null;
	//	}

	//-------------------------------------------------------------------
	private static void removeTextEntry(final IIOMetadata metadata, final String key) throws IIOInvalidTreeException {
		IIOMetadataNode orig = (IIOMetadataNode) metadata.getAsTree(IIOMetadataFormatImpl.standardMetadataFormatName);

		// Remove old nodes with this keyword
		NodeList list = orig.getElementsByTagName("TextEntry");
		for (int i=0; i<list.getLength(); i++) {
			IIOMetadataNode oldNode = (IIOMetadataNode)list.item(i);
			if (oldNode.getAttribute("keyword").equals(key)) {
				oldNode.getParentNode().removeChild(oldNode);
			}
		}

		metadata.setFromTree(IIOMetadataFormatImpl.standardMetadataFormatName, orig);
	}

	//-------------------------------------------------------------------
	private static void replaceTextEntry(final IIOMetadata metadata, final String key, final String value) throws IIOInvalidTreeException {
		IIOMetadataNode orig = (IIOMetadataNode) metadata.getAsTree(IIOMetadataFormatImpl.standardMetadataFormatName);
		removeTextEntry(metadata, key);
		// Remove old nodes with this keyword
		NodeList list = orig.getElementsByTagName("TextEntry");
		for (int i=0; i<list.getLength(); i++) {
			IIOMetadataNode oldNode = (IIOMetadataNode)list.item(i);
			if (oldNode.getAttribute("keyword").equals(key)) {
				oldNode.getParentNode().removeChild(oldNode);
			}
		}

		// Add new node
		IIOMetadataNode textEntry = new IIOMetadataNode("TextEntry");
		textEntry.setAttribute("keyword", key);
		textEntry.setAttribute("value", value);

		IIOMetadataNode text = new IIOMetadataNode("Text");
		text.appendChild(textEntry);

		orig.appendChild(text);

		metadata.setFromTree(IIOMetadataFormatImpl.standardMetadataFormatName, orig);
	}

	//-------------------------------------------------------------------
	private static void addTextEntry(final IIOMetadata metadata, final String key, final String value) throws IIOInvalidTreeException {
		IIOMetadataNode textEntry = new IIOMetadataNode("TextEntry");
		textEntry.setAttribute("keyword", key);
		textEntry.setAttribute("value", value);

		IIOMetadataNode text = new IIOMetadataNode("Text");
		text.appendChild(textEntry);

		IIOMetadataNode root = new IIOMetadataNode(IIOMetadataFormatImpl.standardMetadataFormatName);
		root.appendChild(text);

		metadata.mergeTree(IIOMetadataFormatImpl.standardMetadataFormatName, root);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#writeMetadata(org.prelle.rpgframework.media.metadata.SerializableMetadata, java.io.InputStream, java.io.OutputStream)
	 */
	@Override
	public void writeMetadata(SerializableMetadata meta, Path imageFile) throws IOException {
		logger.debug("START writeMetadata");
		String suffix = imageFile.getFileName().toString();
		suffix = suffix.substring(suffix.lastIndexOf(".")+1, suffix.length());
		Path temporarySrc = Files.createTempFile("babylon", suffix);
		// Copy file
		Files.copy(new FileInputStream(imageFile.toFile()), temporarySrc, StandardCopyOption.REPLACE_EXISTING);

		try (ImageInputStream input = ImageIO.createImageInputStream(new FileInputStream(temporarySrc.toFile()));
				ImageOutputStream output = ImageIO.createImageOutputStream(new FileOutputStream(imageFile.toFile()))) {

			byte[] thumbnail = MetadataUtil.createThumbnailFromImage(new FileInputStream(temporarySrc.toFile()));

			/*
			 * Read image
			 */
			Iterator<ImageReader> readers = ImageIO.getImageReaders(input);
			if (!readers.hasNext())
				return;
			ImageReader reader = readers.next(); // TODO: Validate that there are readers
			reader.setInput(input, true, false);
			IIOImage image = reader.readAll(0, null);

			/*
			 * Modify image
			 */
			if (meta.getArtist()   !=null)  replaceTextEntry(image.getMetadata(), ARTIST, meta.getArtist());
			if (meta.getCategory() !=null)  replaceTextEntry(image.getMetadata(), CATEGORY, meta.getCategory().name());
			if (meta.getCopyright()!=null)  replaceTextEntry(image.getMetadata(), COPYRIGHT, meta.getCopyright());
			if (meta.getLicense()  !=null)  replaceTextEntry(image.getMetadata(), LICENSE, meta.getLicense().name());
			if (meta.getSeries()   !=null)  replaceTextEntry(image.getMetadata(), SERIES, meta.getSeries());
			if (meta.getTitle()    !=null)  replaceTextEntry(image.getMetadata(), TITLE, meta.getTitle());
			if (meta.getSourceURL()!=null)  replaceTextEntry(image.getMetadata(), SOURCE_URL, meta.getSourceURL().toExternalForm());
			if (meta.getUUID()     !=null)  replaceTextEntry(image.getMetadata(), UUID, meta.getUUID().toString());
			if (meta.getKeywords() !=null) {
				removeTextEntry(image.getMetadata(), KEYWORDS);
				for (String word : meta.getKeywords()) {
					addTextEntry(image.getMetadata(), KEYWORDS, word);
				}
			}
			replaceTextEntry(image.getMetadata(), THUMBNAIL, Base64.getEncoder().encodeToString(thumbnail));

			/*
			 * Write image
			 */
			ImageWriter writer = ImageIO.getImageWriter(reader); // TODO: Validate that there are writers
			if (writer==null)
				return;
			writer.setOutput(output);
			writer.write(image);
		} catch (IOException e) {
			logger.error("Failed writing metadata",e);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.media.metadata.MetadataExtractor#writeThumbnail(byte[], java.nio.file.Path)
	 */
	@Override
	public void writeThumbnail(byte[] data, Path imageFile) throws IOException {
		logger.debug("START writeThumbnail");
		String suffix = imageFile.getFileName().toString();
		suffix = suffix.substring(suffix.lastIndexOf(".")+1, suffix.length());
		Path temporarySrc = Files.createTempFile("babylon", suffix);
		// Copy file
		Files.copy(new FileInputStream(imageFile.toFile()), temporarySrc, StandardCopyOption.REPLACE_EXISTING);

		try (ImageInputStream input = ImageIO.createImageInputStream(new FileInputStream(temporarySrc.toFile()));
				ImageOutputStream output = ImageIO.createImageOutputStream(new FileOutputStream(imageFile.toFile()))) {

			/*
			 * Read image
			 */
			Iterator<ImageReader> readers = ImageIO.getImageReaders(input);
			if (!readers.hasNext())
				return;
			ImageReader reader = readers.next(); // TODO: Validate that there are readers
			reader.setInput(input, true, false);
			IIOImage image = reader.readAll(0, null);

			/*
			 * Modify image
			 */
			replaceTextEntry(image.getMetadata(), THUMBNAIL, Base64.getEncoder().encodeToString(data));

			/*
			 * Write image
			 */
			ImageWriter writer = ImageIO.getImageWriter(reader); // TODO: Validate that there are writers
			if (writer==null)
				return;
			writer.setOutput(output);
			writer.write(image);
		} catch (IOException e) {
			logger.error("Failed writing metadata",e);
		}
	}

	//-------------------------------------------------------------------
	public byte[] readThumbnail(Path localFile) {
		logger.debug("START: readThumbnail");
		try {
			MetadataEditor mediaMeta = MetadataEditor.createFrom(localFile.toFile());
			Map<Integer, MetaValue> itunesMeta = mediaMeta.getItunesMeta();
			if (itunesMeta != null) {
				for (Entry<Integer, MetaValue> entry : itunesMeta.entrySet()) {
					String key = fourccToString(entry.getKey());
					switch (key) {
					case "covr":
						return entry.getValue().getData();
					}
				}
			}
		} catch (Exception e) {
			logger.warn("Failed obtaining thumbnail from "+localFile+": "+e);
		} finally {
			logger.debug("STOP : readThumbnail");
		}
		return null;
	}

}
