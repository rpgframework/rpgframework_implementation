/**
 * 
 */
package org.prelle.rpgframework.media;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.prelle.rpgframework.media.metadata.SerializableMetadata;

import de.rpgframework.media.ImageMedia;
import de.rpgframework.media.ImageType;

/**
 * @author prelle
 *
 */
public class ImageMediaImpl extends AbstractMedia implements ImageMedia {

	private int dpi;
	private int width;
	private int height;
	private ImageType type;
	
	//-------------------------------------------------------------------
	public ImageMediaImpl(Path file, Category cat) {
		this.category = cat;
		path = file;
	}
	
	//-------------------------------------------------------------------
	public ImageMediaImpl(SerializableMetadata meta) {
		super(meta);
		dpi = meta.getDPI();
		width = meta.getWidth();
		height= meta.getHeight();
	}

	//-------------------------------------------------------------------
	public String toString() {
		List<String> tmp = new ArrayList<>();
		if (dpi>72) tmp.add("dpi="+dpi);
		return super.toString()+"("+String.join(", ", tmp)+")";
	}

	//-------------------------------------------------------------------
	public byte[] getBytes() {
		try {
			return Files.readAllBytes(path);
		} catch (IOException e) {
			return null;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.ImageMedia#getDPI()
	 */
	@Override
	public int getDPI() {
		return dpi;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.ImageMedia#setDPI(int)
	 */
	@Override
	public void setDPI(int dpi) {
		if (this.dpi!=dpi)
			dirty = true;
		this.dpi = dpi;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.ImageMedia#getWidth()
	 */
	@Override
	public int getWidth() {
		return width;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.media.ImageMedia#getHeight()
	 */
	@Override
	public int getHeight() {
		return height;
	}

	//-------------------------------------------------------------------
	/**
	 * @param width the width to set
	 */
	public void setWidth(int width) {
		this.width = width;
		dirty = true;
	}

	//-------------------------------------------------------------------
	/**
	 * @param height the height to set
	 */
	public void setHeight(int height) {
		this.height = height;
		dirty = true;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public ImageType getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(ImageType type) {
		this.type = type;
		dirty = true;
	}

}
