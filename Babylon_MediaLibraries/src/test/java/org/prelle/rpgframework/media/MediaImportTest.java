/**
 * 
 */
package org.prelle.rpgframework.media;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import javax.swing.SwingUtilities;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.rpgframework.media.LibraryDefinition;
import org.prelle.rpgframework.media.LocalFSMediaLibrary;

import de.rpgframework.media.ImageMedia;
import de.rpgframework.media.ImageType;
import de.rpgframework.media.LicenseType;
import de.rpgframework.media.Media;
import de.rpgframework.media.RoleplayingMetadata.Category;
import de.rpgframework.media.WritableMediaLibrary;
import de.rpgframework.media.map.Battlemap;
import javafx.embed.swing.JFXPanel;

/**
 * @author prelle
 *
 */
public class MediaImportTest {
	
	private final static Logger logger = LogManager.getLogger("test");
	
	private static Path dir;
	private WritableMediaLibrary library;

	//-------------------------------------------------------------------
	protected static void setupJavaFX() throws RuntimeException {
		final CountDownLatch latch = new CountDownLatch(1);
		try {
			SwingUtilities.invokeAndWait(() -> {
				new JFXPanel(); // initializes JavaFX environment
				latch.countDown();
			});
		} catch (InvocationTargetException | InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
//		BasicConfigurator.configure();
//		Enumeration<Appender> app = Logger.getRootLogger().getAllAppenders();	
//		while (app.hasMoreElements()) {
//			Appender a = app.nextElement();
//			a.setLayout(new PatternLayout("%5p [%c] (%F:%L) - %m%n"));
//		}
		dir = Files.createTempDirectory("babylon-medialib-test");
		logger.debug("Created "+dir);
		
//		setupJavaFX();
	}

	//-------------------------------------------------------------------
	private static void deleteDirContent(Path value) throws Exception {
		for (Path child : Files.newDirectoryStream(value)) {
			if (Files.isDirectory(child))
				deleteDirContent(child);
			Files.delete(child);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		if (Files.isDirectory(dir))
			deleteDirContent(dir);
		Files.deleteIfExists(dir);
		logger.debug("Deleted "+dir);
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		LibraryDefinition libDef = new LibraryDefinition();
		libDef.setName("Testlibrary");
		library = new LocalFSMediaLibrary(libDef, dir);
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		((LocalFSMediaLibrary)library).close();
	}

	//-------------------------------------------------------------------
	@Test
	public void testLocalFile1() throws Exception {
		Media media = library.importURL(Category.BATTLEMAP_STATIC, ClassLoader.getSystemResource("Map_with_minimal_EXIF.jpg")).get(0);
		assertNotNull(media);
		assertNotNull(media.getUUID());
		assertEquals(Paths.get("battlemap_static", "Map_with_minimal_EXIF.jpg"), media.getRelativePath());
		assertEquals("Map with minimal EXIF", media.getName());
		assertEquals(ImageType.STATIC, ((ImageMedia)media).getType());
		assertTrue( media instanceof Battlemap);
		Battlemap map = (Battlemap)media;
		assertEquals(200, map.getDPI());
	}

	//-------------------------------------------------------------------
	@Test
	public void testLocalFile2() throws Exception {
		Media media = library.importURL(Category.BATTLEMAP_STATIC, ClassLoader.getSystemResource("Map_with_layer.pdf")).get(0);
		assertNotNull(media);
		assertNotNull(media.getUUID());
		assertEquals(Paths.get("battlemap_static", "Map_with_layer.pdf"), media.getRelativePath());
		assertEquals("Map with layer", media.getName());
		assertTrue( media instanceof Battlemap);
		Battlemap map = (Battlemap)media;
		assertNotNull(map.getLayer());
		assertTrue(map.getLayer().size()>2);
	}

	//-------------------------------------------------------------------
//	@Test
	public void testDeviantArtPageURLFullDownload() throws Exception {
		URL url = new URL("https://artsbyjapao.deviantart.com/art/BEACH-BATTLEMAP-GRID-25x15-554244573");
		Media media = library.importURL(Category.BATTLEMAP_STATIC, url).get(0);
		assertNotNull(media);
		assertNotNull(media.getUUID());
		assertEquals(Paths.get("battlemap_static", "beach_battlemap_grid_25x15_by_artsbyjapao-d95zdul.jpg"), media.getRelativePath());
		assertEquals("BEACH BATTLEMAP GRID 25x15", media.getName());
		assertEquals(null, media.getCopyright());
		assertEquals(LicenseType.CC_BY_NC_SA, media.getLicense());
		assertNotNull(media.getKeywords());

		assertTrue( media instanceof ImageMedia);
		assertEquals(2000, ((ImageMedia)media).getWidth());
		assertEquals(1200, ((ImageMedia)media).getHeight());
		assertEquals(72, ((ImageMedia)media).getDPI());
		assertTrue(media.getKeywords().size()>2);
		assertNotNull(media.getSource());
		assertEquals(url.toExternalForm(), media.getSource().toString());
	}

	//-------------------------------------------------------------------
	@Test
	public void testDeviantArtPageURL() throws Exception {
		URL url = new URL("https://yamaorce.deviantart.com/art/PF-Signifier-605792792");
		Media media = library.importURL(Category.NPC, url).get(0);
		assertNotNull(media);
		assertNotNull(media.getUUID());
		assertEquals(Paths.get("npc", "pf_signifier_by_yamaorce-da0o8pk.jpg"), media.getRelativePath());
		assertEquals("PF Signifier", media.getName());
		assertEquals("2016 by YamaOrce (https://yamaorce.deviantart.com)", media.getCopyright());
		assertTrue( media instanceof ImageMedia);
		assertNotNull(media.getKeywords());
		assertTrue(media.getKeywords().size()>2);
		assertNotNull(media.getSource());
		assertEquals(url.toExternalForm(), media.getSource().toString());
	}

	//-------------------------------------------------------------------
	@Test
	public void testPinterestPageURL() throws Exception {
		URL url = new URL("https://www.pinterest.de/pin/717901996823631314/");
		Media media = library.importURL(Category.NPC, url).get(0);
		assertNotNull(media);
		assertNotNull(media.getUUID());
		assertEquals(Paths.get("npc", "acfa384253744ed5c68674aaa56c7110.jpg"), media.getRelativePath());
		assertEquals("Tavern Map by jasonjuta", media.getName());
		assertNull(media.getCopyright());
		assertTrue( media instanceof ImageMedia);
		assertNotNull(media.getKeywords());
		assertTrue(media.getKeywords().size()==0);
		assertNotNull(media.getSource());
		assertEquals(url.toExternalForm(), media.getSource().toString());
	}

	//-------------------------------------------------------------------
	@Test
	public void testFlickrPhotoListPageURL() throws Exception {
		URL url = new URL("https://www.flickr.com/photos/153079947@N03/36121282086/in/photolist-X2V1HC-8GJFkS-tj8BP-4UXCNo-dGYdKr-c6fSts-8GJRZd-8GJQwC-8GJRb7-8GFzyg-8GJPc3-8GFErD-8GJJxQ-8GFEtF-5Ycfnm-8GFFdZ-8GJJ5h-8GFvzg-8GJRTq-8GFv6e-8GJRH7-58kXM-8GFA6x-buK5Sx-8GJJfd-tj8BR-8GFEgv-8GFEe8-8GFzuZ-aBbzCw-8GJFps-53YNvB-62us7q-5443Gm-XjDwJ-5443DN-TxCiAB-dK6sZ7-5443KC-5443J1-aKbVR8-kpdNbi-ooaHvw-8GFAtr-5SQBnr-noz6Km-bCpN31-nTsgTL-9fUPVH-nELBnv/");
		Media media = library.importURL(Category.BATTLEMAP_STATIC, url).get(0);
		assertNotNull(media);
		assertNotNull(media.getUUID());
		assertEquals(Paths.get("battlemap_static", "36121282086_d084c55c02_b.jpg"), media.getRelativePath());
		assertEquals("river_crossing_battlemap_by_evile_eagle-d4ea4q4", media.getName());
//		assertEquals("2016 by YamaOrce (https://yamaorce.deviantart.com)", media.getCopyright());
		assertTrue( media instanceof ImageMedia);
		assertNotNull(media.getKeywords());
		assertNotNull(media.getSource());
		assertEquals(url.toExternalForm(), media.getSource().toString());
	}

	//-------------------------------------------------------------------
	@Test
	public void testFlickrPhotoPageURLManyTags() throws Exception {
		URL url = new URL("https://www.flickr.com/photos/52753292@N04/36862447834");
		Media media = library.importURL(Category.BATTLEMAP_STATIC, url).get(0);
		assertNotNull(media);
		assertNotNull(media.getUUID());
		assertEquals(Paths.get("battlemap_static", "36862447834_d6c7f095ed_b.jpg"), media.getRelativePath());
		assertEquals("Dungeon Tiles Set 13 - Clearing & Forest Path", media.getName());
//		assertEquals("2016 by YamaOrce (https://yamaorce.deviantart.com)", media.getCopyright());
		assertTrue( media instanceof ImageMedia);
		assertNotNull(media.getKeywords());
		// Image not found - does not recognize kewords
//		assertTrue(media.getKeywords().size()>2);
		assertNotNull(media.getSource());
		assertEquals(url.toExternalForm(), media.getSource().toString());
	}

	//-------------------------------------------------------------------
//	@Test
	public void testPatreonLocked() throws Exception {
		URL url = new URL("https://www.patreon.com/posts/savage-dungeon-16479377?utm_medium=post_notification_email&utm_source=post_link&utm_campaign=patron_engagement&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyZWRpc19rZXkiOiJpbnN0YW50LWFjY2VzczpkYjYzMTdmYS0zMzdlLTRjZWEtOGJlYy05ZGJkOWRhNzJkYmMifQ.utJbjoxg_DPeHQSU7RZbZBxgSWgHh4ucFklVcc5iJ3I");
		try {
			library.importURL(Category.BATTLEMAP_STATIC, url).get(0);
			fail("Patreons only state not detected");
		} catch (IllegalArgumentException e) {
		}
	}

	//-------------------------------------------------------------------
//	@Test
	public void testPatreonFree() throws Exception {
		URL url = new URL("https://www.patreon.com/posts/roadside-tiling-16449434");
		List<Media> medias = library.importURL(Category.BATTLEMAP_STATIC, url);
		assertNotNull(medias);
		assertEquals(2, medias.size());
		Media media = medias.get(0);
		assertNotNull(media.getUUID());
		assertEquals(Paths.get("battlemap_static", "RoadsideTilingGrass.zip"), media.getRelativePath());
		assertEquals("The Roadside Tiling Road & Grass", media.getName());
//		assertEquals("2016 by YamaOrce (https://yamaorce.deviantart.com)", media.getCopyright());
		assertTrue( media instanceof ImageMedia);
		assertNotNull(media.getKeywords());
		// Image not found - does not recognize kewords
		assertTrue(media.getKeywords().size()==1);
		assertEquals("battle map", media.getKeywords().get(0));
		assertNotNull(media.getSource());
		assertEquals(url.toExternalForm(), media.getSource().toString());
	}

	//-------------------------------------------------------------------
	@Test
	public void testVideo1() throws Exception {
		Media media = library.importURL(Category.BATTLEMAP_STATIC, ClassLoader.getSystemResource("Underdark_fissures_grid_LN.m4v")).get(0);
		assertNotNull(media.getUUID());
		assertEquals(Paths.get("battlemap_static", "Underdark_fissures_grid_LN.m4v"), media.getRelativePath());
		assertEquals("An underdark encounter scene", media.getName());
		assertEquals("Nemes Tamas", media.getArtist());
		assertTrue( media instanceof ImageMedia);
		assertEquals(72, ((ImageMedia)media).getDPI());
		assertEquals(1920, ((ImageMedia)media).getWidth());
		assertEquals(1080, ((ImageMedia)media).getHeight());
		assertEquals(ImageType.VIDEO, ((ImageMedia)media).getType());
		assertNotNull(media.getKeywords());
		// Image not found - does not recognize kewords
		assertTrue(media.getKeywords().size()==0);
		assertNotNull(media.getSource());
	}

	//-------------------------------------------------------------------
//	@Test
	public void testVideo2() throws Exception {
		URL url = new URL("https://www.patreon.com/posts/goblin-ambush-16182841");
		List<Media> medias = library.importURL(Category.BATTLEMAP_STATIC, url);
		assertNotNull(medias);
		assertEquals(6, medias.size());
		Media media = medias.get(3);
		assertNotNull(media.getUUID());
		assertEquals(Paths.get("battlemap_static", "Underdark_fissures_grid_LN.m4v"), media.getRelativePath());
		assertEquals("An underdark encounter scene", media.getName());
		assertEquals("Nemes Tamas", media.getArtist());
		assertTrue( media instanceof ImageMedia);
		assertEquals(72, ((ImageMedia)media).getDPI());
		assertEquals(1920, ((ImageMedia)media).getWidth());
		assertEquals(1080, ((ImageMedia)media).getHeight());
		assertEquals(ImageType.VIDEO, ((ImageMedia)media).getType());
		assertNotNull(media.getKeywords());
		// Image not found - does not recognize kewords
		assertTrue(media.getKeywords().size()==0);
		assertNotNull(media.getSource());
	}

	//-------------------------------------------------------------------
//	@Test
	public void testMP3() throws Exception {
		Media media = library.importURL(Category.MUSIC, ClassLoader.getSystemResource("05 A Horse With No Name.mp3")).get(0);
		assertNotNull(media.getUUID());
		assertEquals(Paths.get("battlemap_static", "Underdark_fissures_grid_LN4.m4v"), media.getRelativePath());
		assertEquals("The Roadside Tiling Road & Grass", media.getName());
//		assertEquals("2016 by YamaOrce (https://yamaorce.deviantart.com)", media.getCopyright());
		assertTrue( media instanceof ImageMedia);
		assertNotNull(media.getKeywords());
		// Image not found - does not recognize kewords
		assertTrue(media.getKeywords().size()==1);
		assertEquals("battle map", media.getKeywords().get(0));
		assertNotNull(media.getSource());
	}

}
