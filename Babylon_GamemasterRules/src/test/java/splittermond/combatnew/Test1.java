package splittermond.combatnew;

import org.prelle.rpgframework.support.combatnew.CombatRunnerImpl;

public class Test1 {

	public static void main(String[] args) {
		SplittermondCombatant char1 = new SplittermondCombatant("Lenkan", 35, 7, 8, 12, 20);
		SplittermondCombatant char2 = new SplittermondCombatant("Rattling", 15, 6, 5, 8, 15);
		
		SplittermondCombat combat = new SplittermondCombat();
		combat.addCombatant(char1, new SimpleCombatantPlayer());
		combat.addCombatant(char2, new SimpleCombatantPlayer());
		
		CombatRunnerImpl runner = new CombatRunnerImpl(combat);
		
		combat.start(runner);
		
		for (int i=0; i<3; i++)
			runner.nextStep();
	}

}
