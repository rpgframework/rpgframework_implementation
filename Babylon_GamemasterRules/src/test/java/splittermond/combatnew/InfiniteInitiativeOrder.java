/**
 * 
 */
package splittermond.combatnew;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.rpgframework.support.combatnew.ICombatant;
import org.prelle.rpgframework.support.combatnew.IInitiativeOrder;

/**
 * @author spr
 *
 */
public class InfiniteInitiativeOrder<C extends ICombatant> implements IInitiativeOrder<C> {

	private final static Logger logger = LogManager.getLogger("rpgframework.combat");
	
	class IniOrder implements Comparable<IniOrder> {
		int value;
		List<ICombatant> combatants;
		
		public IniOrder(int val, ICombatant toAdd) {
			this.value = val;
			combatants = new ArrayList<>();
			combatants.add(toAdd);
		}
		public String toString() { return value+"="+combatants; }
		//-------------------------------------------------------------------
		@Override
		public int compareTo(InfiniteInitiativeOrder<C>.IniOrder other) {
			return ((Integer)value).compareTo(other.value);
		}
	}

	private Map<Integer, IniOrder> order;	
	
	//-------------------------------------------------------------------
	public InfiniteInitiativeOrder() {
		order = new HashMap<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.support.combatnew.IInitiativeOrder#setINIRoll(org.prelle.rpgframework.support.combatnew.ICombatant, int)
	 */
	@Override
	public void setINI(ICombatant combatant, int value) {
		IniOrder phase = order.get((Integer)value);
		if (phase==null) {
			phase = new IniOrder(value, combatant);
			order.put(value, phase);
		} else {
			phase.combatants.add(combatant);
		}
	}

	//-------------------------------------------------------------------
	public IniOrder getPhaseOf(ICombatant comb) {
		for (Entry<Integer, IniOrder> entry : order.entrySet()) {
			if (entry.getValue().combatants.contains(comb))
				return entry.getValue();
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.support.combatnew.IInitiativeOrder#getNext()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public C getNext() {
		// TODO Auto-generated method stub
		List<IniOrder> phases = new ArrayList<>(order.values());
		Collections.sort(phases);
		if (phases.isEmpty()) {
			logger.error("No INI phases anymore");
			return null;
		}
		
		// Earliest phase
		IniOrder phase = phases.get(0);
		logger.debug("now phase "+phase);
		
		ICombatant combatant = phase.combatants.get(0);
		// Player is removed from phase in addToINI()
		
		return (C) combatant;
	}
	
	public void addToINI(ICombatant combatant, int value) {
		// Get Old INI phase
		IniOrder phase = getPhaseOf(combatant);
		int newINI = phase.value + value;
		phase.combatants.remove(combatant);
		if (phase.combatants.isEmpty()) {
			order.remove((Integer)phase.value);
		}
		
		logger.debug("New INI of "+combatant+" is "+newINI);
		setINI(combatant, newINI);
	}

}
