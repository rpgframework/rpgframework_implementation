/**
 * 
 */
package splittermond.combatnew;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.prelle.rpgframework.support.combatnew.ICombatAction;
import org.prelle.rpgframework.support.combatnew.ICombatServer;
import org.prelle.rpgframework.support.combatnew.ICombatantPlayer;

/**
 * @author spr
 *
 */
public class SimpleCombatantPlayer implements ICombatantPlayer {
	
	private Random random = new Random();

	//-------------------------------------------------------------------
	/**
	 */
	public SimpleCombatantPlayer() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.support.combatnew.ICombatantPlayer#presentCombatOptions(java.util.List)
	 */
	@Override
	public void presentCombatOptions(ICombatServer server, UUID token, List<ICombatAction> possible) {
		// Randomize action choice
		int select = random.nextInt(possible.size());
		
		server.decideCombatOption(token, possible.get(select));
	}

	@Override
	public void requestInitiativeRoll(ICombatServer server, UUID token) {
		server.respondInitiativeRoll(token, random.nextInt(6)+1);
	}

}
