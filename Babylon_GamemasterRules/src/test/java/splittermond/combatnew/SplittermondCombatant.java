/**
 * 
 */
package splittermond.combatnew;

import org.prelle.rpgframework.support.combatnew.ICombatant;

/**
 * @author spr
 *
 */
public class SplittermondCombatant implements ICombatant {
	
	private String name;
	private int vitality;
	private int baseINI;
	private int speed;
	private int skill;
	private int defense;

	//-------------------------------------------------------------------
	public SplittermondCombatant(String name, int vitality, int baseINI, int speed, int skill, int vtd) {
		this.name = name;
		this.vitality = vitality;
		this.baseINI = baseINI;
		this.speed = speed;
		this.skill = skill;
		this.defense = vtd;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return name;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.support.combatnew.ICombatant#calculateINI(int)
	 */
	@Override
	public int calculateINI(int roll) {
		return baseINI - roll;
	}

	public String getName() {
		return name;
	}

	public int getVitality() {
		return vitality;
	}

	public int getBaseINI() {
		return baseINI;
	}

	public int getSpeed() {
		return speed;
	}

	public int getSkill() {
		return skill;
	}

	public int getDefense() {
		return defense;
	}

}
