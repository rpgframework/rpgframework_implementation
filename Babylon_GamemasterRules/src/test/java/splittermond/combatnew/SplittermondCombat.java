/**
 * 
 */
package splittermond.combatnew;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.rpgframework.support.combatnew.ICombat;
import org.prelle.rpgframework.support.combatnew.ICombatAction;
import org.prelle.rpgframework.support.combatnew.ICombatRunnerCallback;
import org.prelle.rpgframework.support.combatnew.ICombatant;
import org.prelle.rpgframework.support.combatnew.ICombatantPlayer;
import org.prelle.rpgframework.support.combatnew.IInitiativeOrder;

/**
 * @author spr
 *
 */
public class SplittermondCombat implements ICombat<SplittermondCombatant> {

	private final static Logger logger = LogManager.getLogger("rpgframework.combat");

	private Map<SplittermondCombatant, ICombatantPlayer> combatants;

	private InfiniteInitiativeOrder<SplittermondCombatant> initiative;
	
	private transient ICombatRunnerCallback runner;
	
	//-------------------------------------------------------------------
	/**
	 */
	public SplittermondCombat() {
		combatants = new HashMap<>();
		initiative = new InfiniteInitiativeOrder<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.support.combatnew.ICombat#addCombatant(org.prelle.rpgframework.support.combatnew.ICombatant)
	 */
	@Override
	public void addCombatant(SplittermondCombatant toAdd, ICombatantPlayer player) {
		if (!combatants.containsKey(toAdd))
			combatants.put(toAdd, player);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.support.combatnew.ICombat#getPlayer(org.prelle.rpgframework.support.combatnew.ICombatant)
	 */
	@Override
	public ICombatantPlayer getPlayer(SplittermondCombatant combatant) {
		return combatants.get(combatant);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.support.combatnew.ICombat#getInitiativeOrder()
	 */
	@Override
	public IInitiativeOrder<SplittermondCombatant> getInitiativeOrder() {
		return initiative;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.support.combatnew.ICombat#getPossibleActions(org.prelle.rpgframework.support.combatnew.ICombatant)
	 */
	@Override
	public List<ICombatAction> getPossibleActions(ICombatant next) {
		List<ICombatAction> action = new ArrayList<>();
		action.add(new ActionAttack());
		return action;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.support.combatnew.ICombat#perform(org.prelle.rpgframework.support.combatnew.ICombatant, org.prelle.rpgframework.support.combatnew.ICombatAction)
	 */
	@Override
	public void perform(ICombatant combatant, ICombatAction decision) {
		logger.warn("TODO: perform "+decision);
		SplittermondCombatant comb = (SplittermondCombatant)combatant;
		
		initiative.addToINI(combatant, comb.getSpeed());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.support.combatnew.ICombat#start()
	 */
	@Override
	public void start(ICombatRunnerCallback runner) {
		this.runner = runner;
		// TODO Auto-generated method stub
		for (SplittermondCombatant combatant : combatants.keySet()) {
			ICombatantPlayer player = combatants.get(combatant);
			// Roll initiative
			player.requestInitiativeRoll(runner, runner.generateToken(combatant));
		}
	}

}
