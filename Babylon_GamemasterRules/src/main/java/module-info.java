/**
 * @author Stefan Prelle
 *
 */
module babylon.gamemaster {
	exports org.prelle.rpgframework.support.combat;
	exports org.prelle.rpgframework.support.combat.map;

	requires babylon.media;
	requires babylon;
	requires org.apache.logging.log4j;
	requires rpgframework.api;
}