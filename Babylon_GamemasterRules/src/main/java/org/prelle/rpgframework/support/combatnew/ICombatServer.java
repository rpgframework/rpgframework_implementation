/**
 * 
 */
package org.prelle.rpgframework.support.combatnew;

import java.util.UUID;

/**
 * @author spr
 *
 */
public interface ICombatServer {

	public void respondInitiativeRoll(UUID token, int value);

	public void decideCombatOption(UUID token, ICombatAction action);
	
}
