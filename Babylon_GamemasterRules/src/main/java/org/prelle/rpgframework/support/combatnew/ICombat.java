/**
 * 
 */
package org.prelle.rpgframework.support.combatnew;

import java.util.List;

/**
 * @author spr
 *
 */
public interface ICombat<C extends ICombatant> {
	
	public void addCombatant(C toAdd, ICombatantPlayer player);
	
	public ICombatantPlayer getPlayer(C toAdd);

	public IInitiativeOrder<C> getInitiativeOrder();
	
	/**
	 * Initialize combatants (rolling initiative)
	 */
	public void start(ICombatRunnerCallback callback);
	

	public List<ICombatAction> getPossibleActions(ICombatant next);

	public void perform(ICombatant combatant, ICombatAction decision);
	
}
