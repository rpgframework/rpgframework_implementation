/**
 * 
 */
package org.prelle.rpgframework.support.combatnew;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author spr
 *
 */
public class CombatRunnerImpl implements ICombatServer, ICombatRunnerCallback {

	private final static Logger logger = LogManager.getLogger("rpgframework.combat");

	private ICombat combat;
	
	private transient ICombatant current;
	private transient List<ICombatAction> possible;
	private transient Map<UUID, ICombatant> validTokens;
	
	//-------------------------------------------------------------------
	public CombatRunnerImpl(ICombat combat) {
		this.combat = combat;
		validTokens = new HashMap<>();
	}

	//-------------------------------------------------------------------
	@Override
	public UUID generateToken(ICombatant combatant) {
		UUID ret = UUID.randomUUID();
		validTokens.put(ret, combatant);
		return ret;
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public void nextStep() {
		current = combat.getInitiativeOrder().getNext();
		logger.debug("Next initiative: "+current);
		// Determine all allowed actions
		possible = combat.getPossibleActions(current);
		logger.debug(current+"'s possible actions: "+possible);
		// Find the user playing a combatant and ask him what to do
		// Response is coming in asynchronously
		ICombatantPlayer player = combat.getPlayer(current);
		logger.debug("Player of "+current+" is "+player);
		player.presentCombatOptions(this, generateToken(current), possible);
	}

	//-------------------------------------------------------------------
	public void processUserDecision(ICombatant combatant, ICombatAction decision) {
		if (!possible.contains(decision))
			throw new IllegalArgumentException("Action was not listed as possible");
		
		combat.perform(combatant, decision);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.support.combatnew.ICombatServer#respondInitiativeRoll(java.lang.String, int)
	 */
	@Override
	public void respondInitiativeRoll(UUID token, int roll) {
		ICombatant combatant = validTokens.remove(token);
		if (combatant==null)
			throw new IllegalArgumentException("Invalid token");
		
		int order = combatant.calculateINI(roll);
		logger.debug(combatant+" rolls initiative "+roll+" which results in INI "+order);
		combat.getInitiativeOrder().setINI(combatant, order);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.support.combatnew.ICombatServer#decideCombatOption(java.util.UUID, org.prelle.rpgframework.support.combatnew.ICombatAction)
	 */
	@Override
	public void decideCombatOption(UUID token, ICombatAction action) {
		ICombatant combatant = validTokens.remove(token);
		if (combatant==null)
			throw new IllegalArgumentException("Invalid token");
		
		logger.debug(combatant+" performs "+action);
		combat.perform(combatant, action);
		
	}

}
