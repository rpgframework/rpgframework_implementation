/**
 * 
 */
package org.prelle.rpgframework.support.combatnew;

/**
 * @author spr
 *
 */
public interface IInitiativeOrder<C extends ICombatant> {

	public void setINI(ICombatant combatant, int value);

	public C getNext();
	
}
