package org.prelle.rpgframework.support.combatnew;

import java.util.List;
import java.util.UUID;

/**
 * The user or AI deciding for the combatant
 */
public interface ICombatantPlayer {
	
	//-------------------------------------------------------------------
	public void requestInitiativeRoll(ICombatServer respondTo, UUID token);

	//-------------------------------------------------------------------
	/**
	 * Present the player a list of possible actions
	 * @param possible
	 */
	public void presentCombatOptions(ICombatServer respondTo, UUID token, List<ICombatAction> possible);

}
