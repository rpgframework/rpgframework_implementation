/**
 * 
 */
package org.prelle.rpgframework.support.combatnew;

/**
 * @author spr
 *
 */
public interface ICombatant {

	//-------------------------------------------------------------------
	/**
	 * Turn a user performed INI roll into an INI value
	 */
	public int calculateINI(int roll);
	
}
