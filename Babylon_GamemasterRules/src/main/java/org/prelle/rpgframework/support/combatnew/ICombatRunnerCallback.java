/**
 * 
 */
package org.prelle.rpgframework.support.combatnew;

import java.util.UUID;

/**
 * @author spr
 *
 */
public interface ICombatRunnerCallback extends ICombatServer {

	//-------------------------------------------------------------------
	public UUID generateToken(ICombatant combatant);
	
}
