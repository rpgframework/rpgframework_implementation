/**
 * @author Stefan Prelle
 *
 */
module babylon {
	exports org.prelle.rpgframework;

	provides de.rpgframework.RPGFramework with org.prelle.rpgframework.RPGFrameworkImpl;
	uses org.prelle.rpgframework.BabylonPlugin;

	requires java.prefs;
	requires org.apache.logging.log4j;
	requires rpgframework.api;
}