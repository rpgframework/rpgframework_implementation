/**
 *
 */
package org.prelle.rpgframework;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

/**
 * @author prelle
 *
 */
public interface BabylonConstants {

	public static PropertyResourceBundle RES = (PropertyResourceBundle)ResourceBundle.getBundle("org/prelle/rpgframework/babylon");

}
