/**
 * @author Stefan Prelle
 *
 */
module babylon.session {
	exports org.prelle.rpgframework.charprov;
	exports org.prelle.rpgframework.session;
	exports org.prelle.rpgframework.webserver;
	exports org.prelle.rpgframework.social;
	exports org.prelle.rpgframework.addressbook;
	exports org.prelle.rpgframework.device;
	exports org.prelle.rpgframework.sessioncore;

	provides org.prelle.rpgframework.BabylonPlugin with org.prelle.rpgframework.session.BabylonSessionManagement;

	requires babylon;
	requires babylon.charrules;
	requires babylon.media;
	requires ez.vcard;
	requires java.desktop;
	requires java.sql;
	requires java.sql.rowset;
	requires java.xml;
	requires javafx.swing;
	requires org.apache.logging.log4j;
	requires org.eclipse.jetty.server;
	requires org.eclipse.jetty.util;
	requires rpgframework.api;
	requires simple.persist;
}