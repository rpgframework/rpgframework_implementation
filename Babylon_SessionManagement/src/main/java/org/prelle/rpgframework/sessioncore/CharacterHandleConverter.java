package org.prelle.rpgframework.sessioncore;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.simplepersist.SerializationException;
import org.prelle.simplepersist.XMLElementConverter;
import org.prelle.simplepersist.marshaller.TextNode;
import org.prelle.simplepersist.marshaller.XmlNode;
import org.prelle.simplepersist.unmarshal.XMLTreeItem;

import de.rpgframework.RPGFramework;
import de.rpgframework.RPGFrameworkLoader;
import de.rpgframework.character.CharacterHandleFull;
import de.rpgframework.core.Player;

//-------------------------------------------------------------------
public class CharacterHandleConverter implements XMLElementConverter<CharacterHandleFull> {

	private final static  Logger logger = LogManager.getLogger("babylon");

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.Converter#write(org.prelle.simplepersist.marshaller.XmlNode, java.lang.Object)
	 */
	@Override
	public void write(XmlNode node, CharacterHandleFull value) throws Exception {
		TextNode name = new TextNode("charref",null, value.getOwner().getId()+"/"+value.getName());
		node.getChildren().add(name);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.Converter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public CharacterHandleFull read(XMLTreeItem node, StartElement ev, XMLEventReader evRd) throws Exception {
		XMLEvent charRefEv = evRd.nextEvent();
		if (charRefEv.isCharacters())
			charRefEv = evRd.nextEvent();
		if (!charRefEv.isStartElement() || !charRefEv.asStartElement().getName().getLocalPart().equals("charref"))
			throw new SerializationException("Expected <charref> event after "+ev+" but got "+charRefEv);
		
		// Next event should be a character event
		XMLEvent charEv = evRd.nextEvent();
		if (!charEv.isCharacters())
			throw new SerializationException("Expected CDATA event after "+charRefEv+" but got "+charEv);
		
		
		String id = charEv.asCharacters().getData();
		logger.trace("Search "+id);
		
		StringTokenizer tok = new StringTokenizer(id, "/");
		if (tok.countTokens()<2)
			throw new SerializationException("Invalid player ID: '"+id+"'.  Must be in format player-id/char-id");
		String playerID = tok.nextToken();
		String charID   = tok.nextToken();
		
		// Read over charref end element
		evRd.nextEvent();
		
		RPGFramework framework = RPGFrameworkLoader.getInstance();
		Player player = framework.getPlayerService().getPlayer(playerID);
		if (player==null)
			throw new NoSuchElementException("Unknown player '"+playerID+"'");
		
		for (CharacterHandleFull handle : framework.getSessionManagement().getCharacterService().getCharacters(player)) {
			logger.trace("    ID = "+handle);
			if (handle.getName().equals(charID)) {
				logger.trace("found character "+handle);
				return handle;
			}
		}

		throw new NoSuchElementException("No character named '"+charID+"' found for player "+player.getId());
	}
	
}