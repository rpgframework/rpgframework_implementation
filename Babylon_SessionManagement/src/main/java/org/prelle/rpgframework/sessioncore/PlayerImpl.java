/**
 * 
 */
package org.prelle.rpgframework.sessioncore;

import java.nio.file.Path;

import org.prelle.rpgframework.addressbook.VCardContact;

import de.rpgframework.addressbook.InfoType;
import de.rpgframework.core.Player;
import ezvcard.VCard;

/**
 * @author prelle
 *
 */
//@Root(name="player")
public class PlayerImpl extends VCardContact implements Player {
	
	private Path file;
	private Path directory;
	
	//--------------------------------------------------------------------
	public PlayerImpl(VCard vcard) {
		super(vcard);
	}
	
	//--------------------------------------------------------------------
	public PlayerImpl(VCard vcard, Path vcardFile, Path directory) {
		super(vcard);
		this.file = vcardFile;
		this.directory = directory;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return super.toString()+"(dir="+directory+",vcf="+file+")";
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.Player#getId()
	 */
	@Override
	public String getId() {
		if (get(InfoType.NICKNAME)!=null)
			return get(InfoType.NICKNAME);
		
		StringBuffer buf = new StringBuffer();
		buf.append(String.valueOf(get(InfoType.FIRSTNAME, null)));
		buf.append(String.valueOf(get(InfoType.LASTNAME, null)));
		return buf.toString();
	}

	//-------------------------------------------------------------------
	public VCard getVCard() {
		return vcard;
	}

	//-------------------------------------------------------------------
	public Path getFile() {
		return file;
	}

	//-------------------------------------------------------------------
	public void setFile(Path file) {
		this.file = file;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.Player#getName()
	 */
	@Override
	public String getName() {
		return getDisplayName();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the directory
	 */
	public Path getDirectory() {
		return directory;
	}

	//-------------------------------------------------------------------
	/**
	 * @param directory the directory to set
	 */
	public void setDirectory(Path directory) {
		this.directory = directory;
	}

}
