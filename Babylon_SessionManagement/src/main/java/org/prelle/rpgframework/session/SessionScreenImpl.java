/**
 * 
 */
package org.prelle.rpgframework.session;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.rpgframework.ConfigContainerImpl;
import org.prelle.rpgframework.media.ImageMediaImpl;

import de.rpgframework.devices.DeviceFunction;
import de.rpgframework.devices.FunctionSessionHandout;
import de.rpgframework.devices.RPGToolDevice;
import de.rpgframework.media.RoleplayingMetadata.Category;
import de.rpgframework.media.Webserver;
import de.rpgframework.session.SessionScreen;

/**
 * @author prelle
 *
 */
@SuppressWarnings("exports")
public class SessionScreenImpl implements SessionScreen, Runnable {
	
	private final static Logger logger = LogManager.getLogger("babylon.sessionscreen");
	
	private List<RPGToolDevice> devices;
	
//	private Instant lastUpdate;
	private Thread thread;
	private List<byte[]> semaphore;
	
	private Webserver webserver;
	private Path remotePath;
	private Path localPath;
	
	//-------------------------------------------------------------------
	/**
	 * @throws IOException 
	 */
	public SessionScreenImpl(ConfigContainerImpl configRoot, Webserver webServ) throws IOException {
		devices = new ArrayList<>();
		semaphore = new ArrayList<>();
		thread = new Thread(this, "SessionScreenImpl");
		thread.setDaemon(true);
		thread.start();
		
		Path tmpDir = Files.createTempDirectory("salomon");
		tmpDir.toFile().deleteOnExit();
		localPath = tmpDir.resolve("gamemaster.jpg");
		remotePath = Paths.get("/gamemaster.jpg");
		
		webserver = webServ;
		webserver.addFile(localPath, remotePath);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.gamemaster.jfx.gamemaster.jfx.SessionScreen#addClient(de.rpgframework.devices.RPGToolDevice)
	 */
	@Override
	public void addClient(RPGToolDevice clientScreen) {
		// TODO Auto-generated method stub
		logger.warn("addClient("+clientScreen+")");
		
		
		if (!clientScreen.getSupportedFunctions().contains(DeviceFunction.SESSION_HANDOUT)) {
			logger.error("Cannot add device "+clientScreen.getName()+" - it does not support showing media");
			throw new IllegalArgumentException("SESSION_HANDOUT not supported");
		}
		
		devices.add(clientScreen);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.gamemaster.jfx.gamemaster.jfx.SessionScreen#removeClient(de.rpgframework.devices.RPGToolDevice)
	 */
	@Override
	public void removeClient(RPGToolDevice clientScreen) {
		// TODO Auto-generated method stub
		logger.warn("removeClient("+clientScreen+")");
		
		devices.remove(clientScreen);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.gamemaster.jfx.gamemaster.jfx.SessionScreen#show(de.rpgframework.media.ImageMedia, de.rpgframework.gamemaster.jfx.gamemaster.jfx.SessionScreen.DisplayHint)
	 */
	@Override
	public void update(byte[] jpgData) {
//		logger.info("-----------------------------------------update ");
		synchronized (semaphore) {
			semaphore.add(jpgData);
			semaphore.notify();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		while (true) {
			byte[] jpgData = null;
			synchronized (semaphore) {
				while (semaphore.isEmpty()) {
					try {
						semaphore.wait();
					} catch (InterruptedException e) {
						logger.error("Interrupted");
						return;
					}
				}

//				logger.info("-----------------------------------------found data");
				jpgData = semaphore.get(semaphore.size()-1);
				semaphore.clear();
			}

//			Instant now = Instant.now();
//			if (lastUpdate!=null && now.minusSeconds(5).isBefore(lastUpdate)) {
//				logger.debug("Ignore update");
//				return;
//			}
//			lastUpdate = now;

//			Path path = new File("gamemaster.jpg").toPath();

			try {
				Files.write(localPath, jpgData);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			
			ImageMediaImpl image = new ImageMediaImpl(localPath, Category.BATTLEMAP_STATIC);
			URL url = webserver.toURL(localPath);
			image.setSource(url);
			for (RPGToolDevice dev : devices) {
				logger.debug("  send to "+dev.getName()+" / "+dev.getClass().getSimpleName()+" // "+dev.getSupportedFunctions()+" ?");
				FunctionSessionHandout handout = (FunctionSessionHandout) dev.getFunction(DeviceFunction.SESSION_HANDOUT);
				handout.showHandout(image);
			}
			
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
			}
		}
	}

}
