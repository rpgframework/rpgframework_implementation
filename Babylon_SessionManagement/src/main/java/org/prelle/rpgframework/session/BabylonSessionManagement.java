/**
 * 
 */
package org.prelle.rpgframework.session;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.rpgframework.BabylonPlugin;
import org.prelle.rpgframework.ConfigContainerImpl;
import org.prelle.rpgframework.RPGFrameworkImpl;
import org.prelle.rpgframework.charprov.BaseCharacterProvider;
import org.prelle.rpgframework.charprov.EventingCharacterProvider;
import org.prelle.rpgframework.device.DeviceServiceImpl;
import org.prelle.rpgframework.sessioncore.PlayerServiceImpl;
import org.prelle.rpgframework.sessioncore.SessionServiceImpl;
import org.prelle.rpgframework.social.InternalSocialNetworkWrapper;
import org.prelle.rpgframework.webserver.WebserverImpl;

import de.rpgframework.ConfigContainer;
import de.rpgframework.FunctionSessionManagement;
import de.rpgframework.RPGFrameworkInitCallback;
import de.rpgframework.RPGFrameworkLoader.FunctionType;
import de.rpgframework.boot.StandardBootSteps;
import de.rpgframework.character.CharacterProviderFull;
import de.rpgframework.core.PlayerService;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.core.SessionService;
import de.rpgframework.devices.DeviceService;
import de.rpgframework.media.Webserver;
import de.rpgframework.session.SessionScreen;
import de.rpgframework.social.SocialNetworkProvider;

/**
 * @author prelle
 *
 */
@SuppressWarnings("unused")
public class BabylonSessionManagement implements BabylonPlugin, FunctionSessionManagement {

	private final static Logger logger = LogManager.getLogger("babylon.session");
	
	private RPGFrameworkInitCallback callback;
	private ConfigContainer configRoot;
	private CharacterProviderFull characterService;
	private DeviceServiceImpl deviceService;
	private PlayerServiceImpl  playerService;
	private SessionServiceImpl sessionService;
	private SessionScreenImpl sessionScreen;
	private Webserver webserver;
	private SocialNetworkProvider socialNetworkService;

	//-------------------------------------------------------------------
	/**
	 */
	public BabylonSessionManagement() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.BabylonPlugin#getType()
	 */
	@Override
	public FunctionType getType() {
		return FunctionType.SESSION_MANAGEMENT;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.BabylonPlugin#initialize(org.prelle.rpgframework.RPGFrameworkImpl, de.rpgframework.ConfigContainer, de.rpgframework.RPGFrameworkInitCallback)
	 */
	@SuppressWarnings("exports")
	@Override
	public void initialize(RPGFrameworkImpl fwImpl, ConfigContainerImpl configRoot, RPGFrameworkInitCallback callback, List<RoleplayingSystem> limit) {
		logger.info("initialize");
		this.callback = callback;
		this.configRoot = configRoot;

		try {
			playerService = new PlayerServiceImpl(configRoot);
			sessionService = new SessionServiceImpl(configRoot);
			characterService = new EventingCharacterProvider(new BaseCharacterProvider(configRoot));
//			characterService = new SyncingCharacterProvider();
//			characterService = new CospaceCharacterProvider();
			deviceService    = new DeviceServiceImpl();
			socialNetworkService = new InternalSocialNetworkWrapper(configRoot);
			webserver        = new WebserverImpl();
			sessionScreen    = new SessionScreenImpl(configRoot, webserver);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
		fwImpl.setSessionManagement(this);
		
		playerService.initialize();
		sessionService.initialize();
		fwImpl.addStepDefinition(StandardBootSteps.GROUP_DATA, new LoadGroupDataBootStep(sessionService, sessionService.getLocalSessionBaseDir()));
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.FunctionSessionManagement#getWebserver()
	 */
	@Override
	public Webserver getWebserver() {
		return webserver;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.FunctionSessionManagement#getSessionScreen()
	 */
	@Override
	public SessionScreen getSessionScreen() {
		return sessionScreen;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.FunctionSessionManagement#getPlayerService()
	 */
	@Override
	public PlayerService getPlayerService() {
		return playerService;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.FunctionSessionManagement#getSessionService()
	 */
	@Override
	public SessionService getSessionService() {
		return sessionService;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.FunctionSessionManagement#getDeviceService()
	 */
	@Override
	public DeviceService getDeviceService() {
		return deviceService;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.FunctionSessionManagement#getSocialNetworkProvider()
	 */
	@Override
	public SocialNetworkProvider getSocialNetworkProvider() {
		return socialNetworkService;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.FunctionSessionManagement#getCharacterService()
	 */
	@Override
	public CharacterProviderFull getCharacterService() {
		return characterService;
	}

}
