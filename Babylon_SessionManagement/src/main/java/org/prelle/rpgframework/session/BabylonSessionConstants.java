/**
 * 
 */
package org.prelle.rpgframework.session;

import java.util.ResourceBundle;

/**
 * @author prelle
 *
 */
public interface BabylonSessionConstants {
	
	public static ResourceBundle BABYLON = ResourceBundle.getBundle("org/prelle/rpgframework/session/i18n/babylon-session");

}
