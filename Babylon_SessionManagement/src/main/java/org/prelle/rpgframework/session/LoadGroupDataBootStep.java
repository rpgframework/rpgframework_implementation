/**
 * 
 */
package org.prelle.rpgframework.session;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.rpgframework.sessioncore.GroupImpl;
import org.prelle.rpgframework.sessioncore.SessionContextImpl;
import org.prelle.rpgframework.sessioncore.SessionServiceImpl;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Serializer;

import de.rpgframework.ConfigOption;
import de.rpgframework.RPGFrameworkInitCallback;
import de.rpgframework.boot.BootStep;
import de.rpgframework.core.Group;
import de.rpgframework.core.Player;
import de.rpgframework.core.SessionContext;

/**
 * @author prelle
 *
 */
public class LoadGroupDataBootStep implements BootStep {

	private final static Logger logger = LogManager.getLogger("babylon.session");
	private final static String GROUP_FILENAME = "group.xml";
	
	private SessionServiceImpl sessServ;
	private Path localSessBaseDir;
	
	private Serializer marshaller;

	//-------------------------------------------------------------------
	/**
	 */
	public LoadGroupDataBootStep(SessionServiceImpl sessServ, Path localSessBaseDir) {
		this.sessServ = sessServ;
		this.localSessBaseDir = localSessBaseDir;
		marshaller = new Persister();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.boot.BootStep#getID()
	 */
	@Override
	public String getID() {
		return "GROUP_DATA";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.boot.BootStep#getWeight()
	 */
	@Override
	public int getWeight() {
		return 5;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.boot.BootStep#shallBeDisplayedToUser()
	 */
	@Override
	public boolean shallBeDisplayedToUser() {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.boot.BootStep#getConfiguration()
	 */
	@Override
	public List<ConfigOption<?>> getConfiguration() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.boot.BootStep#execute(de.rpgframework.RPGFrameworkInitCallback)
	 */
	@Override
	public boolean execute(RPGFrameworkInitCallback callback) {
		List<Group> groups = new ArrayList<Group>();
		try {
			DirectoryStream<Path> dir = Files.newDirectoryStream(localSessBaseDir);
			for (Path groupDir : dir) {
				if (!Files.isDirectory(groupDir))
					continue;
				// Expect sub directory name to be a group name
				String groupName = groupDir.getFileName().toString();
				// Load data into group
				Path path = groupDir.resolve(GROUP_FILENAME);
				if (!Files.exists(path)) {
					logger.warn("Ignore group with properties missing: "+path);
					continue;
				}
				
				logger.fatal("Read group "+path);
				try {
					GroupImpl group = marshaller.read(GroupImpl.class, new FileReader(path.toFile()));
					if (!group.getName().equals(groupName)) {
						logger.warn(path+" is expected to be in a directory named "+groupName);
					}
					group.setInternalData(sessServ, groupDir, path);
					group.setLastChange(Files.getLastModifiedTime(path).toInstant());
					// Add links from session to group
					SessionContext lastSession = null;
					for (SessionContext sess : group.getAdventures()) {
						((SessionContextImpl)sess).setGroup(group);
						for (Player tmp : group) {
							if (sess.getCharacter(tmp)==null)
								logger.warn("No character for "+tmp.getName()+" in session "+sess.getAdventure()+" in group "+group.getName());
						}
						lastSession = sess;
						if (sess.getAdventure()==null) {
							logger.warn("No adventure set in session of group "+group.getName());
						}
					}
					// Add a session time
					if (lastSession!=null && lastSession.getLastTime()==null)
						lastSession.setLastTime(Files.getLastModifiedTime(path).toInstant());
					
					groups.add(group);
					logger.info("Found group "+group);
				} catch (Exception e) {
					logger.error("Failed reading group file "+path,e);
				}
			}
		} catch (IOException e) {
			logger.error("Failed to load all groups",e);
		}
		logger.debug("STOP  loadGroups");
		sessServ.setGroups(groups);
		return true;
	}

}
