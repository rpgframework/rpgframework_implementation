/**
 * 
 */
package org.prelle.rpgframework.device;

import java.util.Arrays;
import java.util.Collection;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.rpgframework.session.BabylonSessionConstants;

import de.rpgframework.devices.DeviceFunction;
import de.rpgframework.devices.RPGToolDevice;

/**
 * @author prelle
 *
 */
public class LocalMediaServer implements RPGToolDevice {

	private final static Logger logger = LogManager.getLogger("babylon.device");
	
	private final static DeviceFunction[] FUNCTIONS = {
		DeviceFunction.MEDIASERVER
	};
	
	private static ResourceBundle BABYLON = BabylonSessionConstants.BABYLON;

	
	
	//-------------------------------------------------------------------
	/**
	 */
	public LocalMediaServer() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.devices.RPGToolDevice#getName()
	 */
	@Override
	public String getName() {
		return BABYLON.getString("localMediaServer.name");
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(RPGToolDevice other) {
		return getName().compareTo(other.getName());
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.devices.RPGToolDevice#getSupportedFunctions()
	 */
	@Override
	public Collection<DeviceFunction> getSupportedFunctions() {
		return Arrays.asList(FUNCTIONS);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.devices.RPGToolDevice#getIcon()
	 */
	@Override
	public byte[] getIcon() {
		// TODO Auto-generated method stub
		return null;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.devices.RPGToolDevice#getFunction(de.rpgframework.devices.DeviceFunction)
	 */
	@Override
	public Object getFunction(DeviceFunction func) {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.devices.RPGToolDevice#activate()
	 */
	@Override
	public void activate() {
		// TODO Auto-generated method stub
		logger.warn("TODO: activate");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.devices.RPGToolDevice#deactivate()
	 */
	@Override
	public void deactivate() {
		// TODO Auto-generated method stub
		logger.warn("TODO: deactivate");
	}

}
