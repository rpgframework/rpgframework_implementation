/**
 * 
 */
package org.prelle.rpgframework.addressbook;

import java.net.URI;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.rpgframework.addressbook.Contact;
import de.rpgframework.addressbook.DefaultIdentityInformation;
import de.rpgframework.addressbook.Identity;
import de.rpgframework.addressbook.IdentityInformation;
import de.rpgframework.addressbook.InfoType;
import de.rpgframework.addressbook.NumberType;
import de.rpgframework.addressbook.Parameter;
import de.rpgframework.addressbook.Scope;
import ezvcard.VCard;
import ezvcard.parameter.ImageType;
import ezvcard.property.Address;
import ezvcard.property.CalendarUri;
import ezvcard.property.Email;
import ezvcard.property.FreeBusyUrl;
import ezvcard.property.Impp;
import ezvcard.property.Nickname;
import ezvcard.property.Photo;
import ezvcard.property.RawProperty;
import ezvcard.property.StructuredName;
import ezvcard.property.Telephone;
import ezvcard.property.VCardProperty;

/**
 * @author prelle
 *
 */
public class VCardContact implements Contact {
	
	private final static Logger logger = LogManager.getLogger("babylon.address");
	
	protected transient VCard vcard;
	private List<URI> knownIDs;
	private byte[] avatar;

	//-------------------------------------------------------------------
	public VCardContact() {
		vcard = new VCard();
		knownIDs = new ArrayList<URI>();
	}

	//-------------------------------------------------------------------
	public VCardContact(VCard card) {
		if (card==null)
			throw new NullPointerException();
		this.vcard = card;
		knownIDs = new ArrayList<URI>();
		getFields();
	}

	//-------------------------------------------------------------------
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append(String.valueOf(get(InfoType.FIRSTNAME, null)));
		buf.append(" ");
		buf.append(String.valueOf(get(InfoType.LASTNAME, null)));
		buf.append(" (");
		buf.append(String.valueOf(get(InfoType.IMPP_JABBER, Scope.PRIVATE)));
		buf.append(")");
		return buf.toString();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.addressbook.Contact#addField(de.rpgframework.addressbook.IdentityInformation)
	 */
	@Override
	public void addField(IdentityInformation field) {
		InfoType type = field.getType();
		String value = field.getValue();
		Scope scope  = null;
		if (field.getParameter(Parameter.SCOPE)!=null)
			scope = Scope.valueOf(field.getParameter(Parameter.SCOPE));
		

		logger.debug(String.format("Set (%s, %s) to %s", String.valueOf(type), String.valueOf(scope), value));
		switch (type) {
		case LASTNAME:
		case FIRSTNAME:
			StructuredName name = vcard.getStructuredName();
			if (name==null) {
				name = new StructuredName();
				vcard.setStructuredName(name);
			}
			switch (type) {
			case LASTNAME: name.setFamily(value); break;
			case FIRSTNAME : name.setGiven(value); break;
			default: 
			}
			break;
		case NICKNAME:
			vcard.setNickname(value);
			break;
		case IMPP_JABBER:
			vcard.addImpp(Impp.xmpp(value));
			break;
		case PHONE:
			Telephone phone = new Telephone(value);
			for (Entry<Parameter, String> entry : field.getParameters()) {
				switch (entry.getKey()) {
				case NUMBER_TYPE:
					NumberType numType = NumberType.valueOf(entry.getValue());
					switch (numType) {
					case FAX   : phone.addParameter("TYPE", "FAX"); break;
					case PSTN  : phone.addParameter("TYPE", "VOICE"); break;
					case MOBILE: phone.addParameter("TYPE", "CELL"); break;
					default:
					}
					break;
				case SCOPE:
					scope = Scope.valueOf(field.getParameter(Parameter.SCOPE));
					switch (scope) {
					case BUSINESS: phone.addParameter("TYPE", "WORK"); break;
					case PRIVATE : phone.addParameter("TYPE", "HOME"); break;
					default:
					}
					break;
				default:
				}
			}
			vcard.addTelephoneNumber(phone);
			break;
		case EMAIL:
			Email mail = new Email(value);
			if (scope!=null) {
				switch (scope) {
				case BUSINESS: mail.addParameter("TYPE", "WORK"); break;
				case PRIVATE : mail.addParameter("TYPE", "HOME"); break;
				default:
				}
			}
			vcard.addEmail(mail);
			break;
		default:
			logger.warn("Don't know how to deal with "+type);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.addressbook.Contact#getFields()
	 */
	@Override
	public List<IdentityInformation> getFields() {
		logger.debug("getFields() of "+vcard);
		List<IdentityInformation> ret = new ArrayList<IdentityInformation>();
		if (vcard.getStructuredName()!=null) {
			ret.add(new DefaultIdentityInformation(InfoType.FIRSTNAME, vcard.getStructuredName().getGiven()));
			ret.add(new DefaultIdentityInformation(InfoType.LASTNAME, vcard.getStructuredName().getFamily()));
		}
		
		// Messaging
//		logger.debug("extended = "+vcard.getExtendedProperties());
//		logger.debug("keys     = "+vcard.getKeys());
//		logger.debug("props    = "+vcard.getProperties());
		for (VCardProperty property : vcard.getProperties()) {
			if (property instanceof StructuredName) {
				StructuredName prop = (StructuredName)property;
				ret.add(new DefaultIdentityInformation(InfoType.FIRSTNAME, prop.getGiven()));
				ret.add(new DefaultIdentityInformation(InfoType.LASTNAME, prop.getFamily()));				
			} else if (property instanceof Telephone) {
				// Telephone				
				Telephone prop = (Telephone)property;
				IdentityInformation field = new DefaultIdentityInformation(InfoType.PHONE, prop.getText());
				
				for (String param : property.getParameters("TYPE")) {
					if (param.equals("CELL"))
						field.setParameter(Parameter.NUMBER_TYPE, NumberType.MOBILE.name());
					else if (param.equals("HOME")) {
						field.setParameter(Parameter.SCOPE, Scope.PRIVATE.name());
					} else if (param.equals("WORK")) {
						field.setParameter(Parameter.SCOPE, Scope.BUSINESS.name());
					} else if (param.equals("VOICE")) {
						field.setParameter(Parameter.NUMBER_TYPE, NumberType.PSTN.name());
					} else if (param.equals("FAX")) {
						field.setParameter(Parameter.NUMBER_TYPE, NumberType.FAX.name());
					} else
						logger.warn("Unknown phone type parameter: "+param);
				}
				// Add
				ret.add(field);
			} else if (property instanceof Email) {
				// Telephone
				Email prop = (Email)property;
				IdentityInformation field = new DefaultIdentityInformation(InfoType.EMAIL, prop.getValue());
				for (String param : property.getParameters("TYPE")) {
					if (param.equals("HOME"))
						field.setParameter(Parameter.SCOPE, Scope.PRIVATE.name());
					else if (param.equals("WORK"))
						field.setParameter(Parameter.SCOPE, Scope.BUSINESS.name());
					else if (param.equals("OTHER"))
						field.setParameter(Parameter.SCOPE, Scope.UNKNOWN.name());
					else
						logger.warn("Unknown mail type parameter: "+param);
				}
				ret.add(field);
			} else if (property instanceof Address) {
				// Address
				Address prop = (Address)property;
				IdentityInformation field = new DefaultIdentityInformation(InfoType.ADDRESS, prop.getLabel());
				logger.debug("Address "+prop.toString()+"  country="+prop.getLabel()+"  street="+prop.getStreetAddress());
				for (String param : property.getParameters("TYPE")) {
					if (param.equals("HOME"))
						field.setParameter(Parameter.SCOPE, Scope.PRIVATE.name());
					else if (param.equals("WORK"))
						field.setParameter(Parameter.SCOPE, Scope.BUSINESS.name());
					else
						logger.warn("Unknown address type parameter: "+param);
				}
				ret.add(field);
			} else if (property instanceof Nickname) {
				// Nickname
				Nickname prop = (Nickname)property;
				for (String val : prop.getValues()) {
					ret.add( new DefaultIdentityInformation(InfoType.NICKNAME, val) );
				}
			} else if (property instanceof RawProperty) {
				RawProperty prop = (RawProperty)property;
				IdentityInformation field = new DefaultIdentityInformation(InfoType.UNKNOWN, prop.getValue());
				logger.debug("  ** rawprop = "+prop.getPropertyName()+" = "+prop.getValue());
				if (prop.getPropertyName().equals("X-JABBER")) {
					field = new DefaultIdentityInformation(InfoType.IMPP_JABBER, prop.getValue());
					URI uri = URI.create("xmpp:"+prop.getValue());
					if (!knownIDs.contains(uri)) {
						knownIDs.add(uri);
					}
				} else if (prop.getPropertyName().equals("X-ICQ")) {
					field = new DefaultIdentityInformation(InfoType.IMPP_ICQ, prop.getValue());
					URI uri = URI.create("icq:"+prop.getValue());
					if (!knownIDs.contains(uri)) {
						knownIDs.add(uri);
					}
				} else if (prop.getPropertyName().equals("X-SKYPE")) {
					field = new DefaultIdentityInformation(InfoType.IMPP_SKYPE, prop.getValue());
					URI uri = URI.create("skype:"+prop.getValue());
					if (!knownIDs.contains(uri)) {
						knownIDs.add(uri);
					}
				} else if (prop.getPropertyName().startsWith("X-EVOLUTION")) {
				} else if (prop.getPropertyName().startsWith("X-MOZILLA")) {
				} else if (prop.getPropertyName().startsWith("X-GDATA-PHOTO-ETAG")) {
				} else
					logger.warn("Not supported: "+prop.getPropertyName()+"="+prop.getValue()+"  params="+property.getParameters());
				// A
				if (field!=null) {
					for (String param : property.getParameters("TYPE")) {
						if (param.equals("HOME"))
							field.setParameter(Parameter.SCOPE, Scope.PRIVATE.name());
						else if (param.equals("WORK"))
							field.setParameter(Parameter.SCOPE, Scope.BUSINESS.name());
						else if (param.equals("OTHER"))
							field.setParameter(Parameter.SCOPE, Scope.UNKNOWN.name());
						else
							logger.warn("Unknown type parameter: "+param);
					}
					ret.add(field);
				}
			} else if (property instanceof Photo) {
				// Photo
				Photo prop = (Photo)property;
				logger.debug("Photo content type = "+prop.getContentType());
				avatar = prop.getData();
			} else if (property instanceof FreeBusyUrl) {
			} else if (property instanceof CalendarUri) {
			} else if (property instanceof Impp) {
				// IMPP
				Impp prop = (Impp)property;
				logger.debug(String.format("  IMPP: Protocol=%s Handle=%s URI=%s", prop.getProtocol(), prop.getHandle(), prop.getUri()));
				if (!knownIDs.contains(prop.getUri())) {
					knownIDs.add(prop.getUri());
				}
			} else {
				logger.debug("  ignored prop: "+property.getClass()+"  params="+property.getParameters());
			}
		}
		
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.addressbook.Identity#getDisplayName()
	 */
	@Override
	public String getDisplayName() {
		if (get(InfoType.NICKNAME, null)!=null)
			return get(InfoType.NICKNAME, null);
		
		StringBuffer buf = new StringBuffer();
		buf.append(String.valueOf(get(InfoType.FIRSTNAME, null)));
		buf.append(" ");
		buf.append(String.valueOf(get(InfoType.LASTNAME, null)));
		return buf.toString();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.addressbook.Identity#getAvatar()
	 */
	@Override
	public byte[] getAvatar() {
		return avatar;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.addressbook.Identity#setTemporaryAvatar(byte[])
	 */
	@Override
	public void setAvatar(byte[] imageData) {
		this.avatar = imageData;
		
		Photo photo = new Photo(imageData, ImageType.JPEG);
		vcard.addPhoto(photo);
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Identity other) {
		return Collator.getInstance().compare(getDisplayName(), other.getDisplayName());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.addressbook.Contact#get(de.rpgframework.addressbook.InfoType)
	 */
	@Override
	public String get(InfoType type) {
		switch (type) {
		case LASTNAME:
			if (vcard.getStructuredName()!=null)
			return vcard.getStructuredName().getFamily();
			if (vcard.getFormattedName()!=null)
				return vcard.getFormattedName().getValue();
			return null;
		case FIRSTNAME:
			if (vcard.getStructuredName()!=null)
				return vcard.getStructuredName().getGiven();
			if (vcard.getFormattedName()!=null)
				return vcard.getFormattedName().getValue();
			return null;
		case NICKNAME:
			if (vcard.getNickname()!=null && !vcard.getNickname().getValues().isEmpty())
				return vcard.getNickname().getValues().get(0);
			return null;
		case IMPP_JABBER:
			for (Impp tmp : vcard.getImpps()) {
				if (tmp.isXmpp())
					return tmp.getUri().toString();
			}
			if (vcard.getExtendedProperty("X-JABBER")!=null) {
				RawProperty prop = vcard.getExtendedProperty("X-JABBER");
				return prop.getValue();
			}
			return null;
		default:
			logger.warn("TODO: don't know how to return "+type);
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.addressbook.Contact#get(de.rpgframework.addressbook.InfoType, de.rpgframework.addressbook.Scope)
	 */
	@Override
	public String get(InfoType type, Scope scope) {
		switch (type) {
		case LASTNAME:
			if (vcard.getStructuredName()!=null)
			return vcard.getStructuredName().getFamily();
			if (vcard.getFormattedName()!=null)
				return vcard.getFormattedName().getValue();
			return null;
		case FIRSTNAME:
			if (vcard.getStructuredName()!=null)
				return vcard.getStructuredName().getGiven();
			if (vcard.getFormattedName()!=null)
				return vcard.getFormattedName().getValue();
			return null;
		case NICKNAME:
			if (vcard.getNickname()!=null && !vcard.getNickname().getValues().isEmpty())
				return vcard.getNickname().getValues().get(0);
			return null;
		case IMPP_JABBER:
			for (Impp tmp : vcard.getImpps()) {
				if (tmp.isXmpp())
					return tmp.getUri().toString();
			}
			if (vcard.getExtendedProperty("X-JABBER")!=null) {
				RawProperty prop = vcard.getExtendedProperty("X-JABBER");
				return prop.getValue();
			}
			return null;
		default:
			logger.warn("TODO: don't know how to return "+type);
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.addressbook.Contact#set(de.rpgframework.addressbook.InfoType, java.lang.String)
	 */
	@Override
	public void set(InfoType type, String value) {
		DefaultIdentityInformation field = new DefaultIdentityInformation(type, value);
		addField(field);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.addressbook.Contact#set(de.rpgframework.addressbook.InfoType, java.lang.String, java.util.Map.Entry[])
	 */
	@Override
	@SuppressWarnings("unchecked")
	public void set(InfoType type, String value,
			Entry<Parameter, String>... params) {
		DefaultIdentityInformation field = new DefaultIdentityInformation(type, value);
		for (Entry<Parameter, String> param :params)
			field.setParameter(param.getKey(), param.getValue());
		
		addField(field);
		
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.addressbook.Contact#getIDs()
	 */
	@Override
	public Collection<URI> getIDs() {
		return new ArrayList<URI>(knownIDs);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.addressbook.Contact#addID(java.net.URI)
	 */
	@Override
	public void addID(URI id) {
		if (!knownIDs.contains(id)) {
			knownIDs.add(id);
			Impp impp = new Impp(id);
			vcard.addImpp(impp);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.addressbook.Contact#removeID(java.net.URI)
	 */
	@Override
	public void removeID(URI id) {
		knownIDs.remove(id);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.addressbook.Contact#containsID(java.net.URI)
	 */
	@Override
	public boolean containsID(URI id) {
		return knownIDs.contains(id);
	}

}
